#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

# """
#   polynomial_optimisation.jl

```@meta
DocTestSetup = quote
    using MotionPlanning
    const mp = MotionPlanning
end
```

"""
    Vertex{Dimension, T <: AbstractFloat} = Dict{Derivatives.Derivative, sa.SVector{Dimension, T}}

A Vertex defines the boundary condition between two trajectories. As such, it must, at minimum, specify a constraint
in position. The key is a [`Derivative`](@ref MotionPlanning.Derivatives) and the value is a Vector (it will be implicitly
converted to a [StaticArrays.SVector](https://github.com/JuliaArrays/StaticArrays.jl)).

# Example
```@example
start = mp.Vertex{dim, Float64}()
start[mp.Derivatives.POSITION] = [1, 1, 1]
midpoint[mp.Derivatives.POSITION] = [2, 0.7, 1.5]
```
"""
const Vertex{Dimension, T <: AbstractFloat} = Dict{Derivatives.Derivative, sa.SVector{Dimension, T}}
const VertexConstraint{Dimension, T <: AbstractFloat} = Pair{Derivatives.Derivative, sa.SVector{Dimension, T}}

Vertex() = Vertex{3, Float64}()

function get_dimension(vertex::Vertex)
    return length(valtype(vertex))
end

abstract type AbstractPolyConstraint{Dimension, T <: AbstractFloat} end
mutable struct PolyConstraint{Dimension, T} <: AbstractPolyConstraint{Dimension, T}
    vertices::Vector{Vertex{Dimension, T}} # the original constraints represented via vertices
    df::Vector{Vector{T}} # d_f in [1] -- compact form of fixed constraints
    dp::Vector{Vector{T}} # d_p in [1] -- compact form of free constraints

    num_vertices::Int
    num_constraints::Int
    num_fixed_constraints::Int
    num_free_constraints::Int
end

function PolyConstraint{Dimension, T}() where {Dimension, T}
    PolyConstraint{Dimension, T}([], [], [], 0, 0, 0, 0)
end
PolyConstraint() = PolyConstraint{3, Float64}()
PolyConstraint(vertices) = PolyConstraint(vertices, [], [], 0, 0, 0, 0)

"""
    PolyOpt{Dimension, T}(N::Int) where {Dimension, T <: AbstractFloat}

PolyOpt implements "Polynomial Trajectory Planning for Aggressive Quadrotor Flight in Dense Indoor Environments" by
Charles Richter, Adam Bry, and Nicholas Roy, which was published in the *Robotics Research, Vol. 114*, Springer International Publishing, 2016, pp. 649–666.
You can find the paper here (possibly outdated): <https://dspace.mit.edu/handle/1721.1/106840#files-area>.

1. `Dimension`: dimension of the problem
1. `T`: primitive type used, must be a subtype of `AbstractFloat`
1. `N`: number of coefficients to use for polynomials -- must be even

# Example
```
julia> po = PolyOpt{3, Float64}(10); # set up a polynomial optimisation problem that has dimension 3, uses `Float64`, with N = 10 coefficients
```
"""
mutable struct PolyOpt{Dimension, T <: AbstractFloat}
    N::Int # number of coefficients
    C::Matrix{T} # C in [1], matrix to reorder free/fixed constraints
    Ainv::Vector{Matrix{T}} # see Ainv in [1] -- vector of NxN inverted mapping matrix for each segment
    Q::Vector{Matrix{T}} # see Q in [1] -- vector of NxN cost matrix for each segment
    constraint::PolyConstraint{Dimension, T} # convenience struct to hold constraints for the optimisation problem

    kHighestDerivativeToOptimise::Int # based on number of coefficients, cannot optimise above this derivative
    derivative_to_optimise::Int

    times::Vector{T}
    num_segments::Int

    function PolyOpt{Dimension, T}(N::Int) where {Dimension, T <: AbstractFloat}
        @assert iseven(N) "N must be even -- instead, N = $N"
        new(N, Matrix{T}(undef, 0, 0), Vector{Matrix{T}}(), Vector{Matrix{T}}(), PolyConstraint{Dimension, T}(), N / 2 - 1)
    end
end

"""
    PolyOpt(dim::Int, N::Int)

Convenience constructor which defaults to `Float64`.
"""
PolyOpt(dim::Int, N::Int) = PolyOpt{dim, Float64}(N)

"""
    PolySol

A `struct` containing meta-data relevant to the solution to the polynomial optimisation problem.

- `N`: number of polynomial coefficients
- `dimension`: dimension of the trajectory
- `derivative_optimised`: the derivative optimised
- `trajectory`: the trajectory, with `derivative_optimised`, that satisfies the original boundary conditions
"""
struct PolySol
    N::Int
    dimension::Int
    derivative_optimised::Derivatives.Derivative
    trajectory::AbstractTrajectory
end

"""
    dimension(polyopt::PolyOpt{Dimension, T})

Return the dimension of the polynomial optimisation problem.
"""
dimension(polyopt::PolyOpt{Dimension, T}) where {Dimension, T} = Dimension

"""
    setN!(prob::PolyOpt, N::Int)

Set the number of polynomial coefficients, `N`.
"""
function setN!(prob::PolyOpt, N::Int)
    @assert iseven(N) "N must be even -- instead, N = $N"
    prob.N = N
    prob.kHighestDerivativeToOptimise = N / 2 - 1
end

"""
    getN(prob::PolyOpt)

Get `N`, the number of polynomial coefficients.
"""
getN(prob::PolyOpt) = prob.N

"""
    get_highest_derivative_to_optimise(prob::PolyOpt)

Get the highest possible derivative that can be optimised, where the relationship to `N`, the number
of polynomial coefficients, is given by: `highest_derivative_to_optimise = N / 2 - 1`.
"""
get_highest_derivative_to_optimise(prob::PolyOpt) = prob.kHighestDerivativeToOptimise

"""
    compute_poly_base_coefficients(N::Int)

Compute a matrix of the base coefficients of an N-th order polynomial.
If you have a polynomial of: f(t) = a0 + a1*t + a2*t^2 + a3*t^3, then
    base_coefficients_ = [1 1 1 1;
                          0 1 2 3;
                          0 0 2 6;
                          0 0 0 6],
where the i-th row corresponds to taking the (i-1)-th derivative and the j-th
column refers to the multiplier for the j-th coefficient.
"""
function compute_poly_base_coefficients(N::Int)
    if N < 1
        throw(DomainError(N, "N must be > 0."))
    end
    base_coefficients = zeros(N, N)
    base_coefficients[1, :] .= 1

    order = N
    for n = 2 : N
        for i = (N - order + 1) : N
            base_coefficients[n, i] = (order - N + i-1) * base_coefficients[n - 1, i]
        end
        order -= 1
    end
    return base_coefficients
end

"""
    compute_poly_coefficients(N::Int, derivative::Int, time::AbstractFloat)

`N`: polynomial order

`derivative`: derivative to compute

`time`: time to evaluate the polynomial

Compute the base coefficients of the `derivative` of an `N`-th order polynomial at `time`.
"""
function compute_poly_coefficients(N::Int, derivative::Derivatives.Derivative, time::AbstractFloat)
    if derivative >= N
        throw(ArgumentError("derivative = $derivative must be < N = $N"))
    end
    if derivative < 0
        throw(DomainError(derivative, "derivative = $derivative must be >= 0"))
    end
    if time < 0
        throw(DomainError(time, "time = $time must be >= 0"))
    end
    base_coeffs = compute_poly_base_coefficients(N)
    coeffs = zeros(N)
    coeffs[derivative + 1] = base_coeffs[derivative + 1, derivative + 1]
    t_power = time
    for j = (derivative + 2) : N
        coeffs[j] = base_coeffs[derivative + 1, j] * t_power
        t_power *= time
    end
    return coeffs
end

"""
    setup(vertices::Vector{Vertex{Dimension, T}},
          times::Vector{T},
          derivative_to_optimise::D) where {Dimension, T <: AbstractFloat, D <: Integer})

Set up a [`PolyOpt`](@ref) problem with the given inputs:
1. `vertices`: a vector of [`Vertex`](@ref), where the i-th element specifies constraints on the derivatives of the i-th vertex
1. `times`: a vector of floats, where the i-th element is the time from the i-th vertex to the (i+1) vertex
1. `derivative_to_optimise`: an integer specifying which derivative to optimise, e.g. 2 denotes the second derivative (acceleration)

Return: a [`PolyOpt`](@ref) problem.
"""
function setup(vertices::Vector{Vertex{Dimension, T}},
               times::Vector{T},
               derivative_to_optimise::D) where {Dimension, T <: AbstractFloat, D <: Integer}
    # this sets the number of coefficients to the bare minimum required to optimise the desired derivative
    N = 2*(derivative_to_optimise + 1)
    prob = PolyOpt{Dimension, T}(N)
    setup!(prob, vertices, times, derivative_to_optimise)
    return prob
end

"""
    setup!(prob::PolyOpt{Dimension, T},
           vertices::Vector{Vertex{Dimension, T}},
           times::Vector{T},
           derivative_to_optimise::D) where {Dimension, T <: AbstractFloat, D <: Integer})

Set up a [`PolyOpt`](@ref) problem with the given inputs:
1. `prob`: a `PolyOpt` that will have its internal state overwritten according to the latest inputs
1. `vertices`: a vector of [`Vertex`](@ref), where the i-th element specifies constraints on the derivatives of the i-th vertex
1. `times`: a vector of floats, where the i-th element is the time from the i-th vertex to the (i+1) vertex
1. `derivative_to_optimise`: an integer specifying which derivative to optimise, e.g. 2 denotes the second derivative (acceleration)
"""
function setup!(prob::PolyOpt{Dimension, T},
                vertices::Vector{Vertex{Dimension, T}},
                times::Vector{T},
                derivative_to_optimise::D) where {Dimension, T <: AbstractFloat, D <: Integer}
    @assert (derivative_to_optimise >= 0) "Derivatives must be non-negative but received: $derivative_to_optimise."
    @assert (derivative_to_optimise <= prob.kHighestDerivativeToOptimise) "Cannot optimise the $derivative_to_optimise-derivative with only $(prob.N) coefficients.
             To optimise the $derivative_to_optimise-derivative, one needs $(2*(derivative_to_optimise + 1)) coefficients."
    @assert ((length(times) + 1) == length(vertices)) "For N vertices, there must be (N - 1) times.\n Instead, there are $(length(vertices)) vertices but $(length(times)) time estimates."

    prob.derivative_to_optimise = derivative_to_optimise
    prob.constraint.vertices = deepcopy(vertices)
    prob.constraint.num_vertices = length(vertices)

    prob.num_segments = prob.constraint.num_vertices - 1
    prob.Ainv = Vector{Matrix{T}}(undef, prob.num_segments)
    prob.Q = Vector{Matrix{T}}(undef, prob.num_segments)
    for i = 1:prob.num_segments
        prob.Q[i] = zeros(T, prob.N, prob.N)
    end

    verify_constraints!(prob.constraint.vertices, prob.kHighestDerivativeToOptimise)
    update_segment_times!(prob, times)
    setup_constraint_reordering_matrix!(prob)
    return Nothing
end

function verify_constraints!(vertices::Vector{Vertex{Dimension, T}}, highest_derivative::Int) where {Dimension, T <: AbstractFloat}
    for (index, vertex) ∈ enumerate(vertices)
        for (derivative, constraint) ∈ vertex
            if derivative > highest_derivative
                @warn "Vertex $index contains a constraint on the $derivative-derivative, but the maximum allowed derivative is $(highest_derivative).\nIgnoring this constraint."
                delete!(vertex, derivative)
            end
        end
    end
    # filter!(vertex->length(vertex)>0, vertices)
end

function update_segment_times!(prob::PolyOpt{Dimension, T}, times::Vector{T}) where {Dimension, T <: AbstractFloat}
    @assert (prob.num_segments == length(times)) "Number of segments must match number of time estimates."
    prob.times = deepcopy(times)
    for (index, segment_time) ∈ enumerate(prob.times)
        @assert (segment_time > 0) "Segment time must be greater than 0."
        compute_quadratic_cost_jacobian!(prob.Q[index], prob.N, prob.derivative_to_optimise, segment_time)
        A = Array{T}(undef, prob.N, prob.N)
        setup_mapping_matrix!(A, segment_time)
        prob.Ainv[index] = inv(A)
        # prob.Ainv[index] = invert_mapping_matrix(A)
    end
end

function compute_quadratic_cost_jacobian!(Q::Matrix{T},
                                          N::Int,
                                          derivative::Int,
                                          time::AbstractFloat) where {T <: AbstractFloat}
    N::Int = LinearAlgebra.checksquare(Q)
    @assert (iseven(N)) "N must be even -- instead, N = $N"
    base_coefficients = compute_poly_base_coefficients(N)
    for col = 0:(N - derivative - 1), row = 0:(N - derivative - 1)
        exponent::T = (N - 1 - derivative) * 2 + 1 - row - col
        Q[N - row, N - col] = base_coefficients[derivative+1, N - row] * base_coefficients[derivative+1, N - col] * time^exponent * 2.0 / exponent
    end
end

function setup_mapping_matrix!(A::Matrix{T}, time::AbstractFloat) where {T <: AbstractFloat}
    N::Int = LinearAlgebra.checksquare(A)
    @assert (iseven(N)) "N must be even -- instead, N = $N"
    for row::Int = 1:N/2
        bottom_half_row::Int = row + N/2
        A[row, :] = compute_poly_coefficients(N, row - 1, 0.0)
        A[bottom_half_row, :] = compute_poly_coefficients(N, row - 1, time)
    end
end

# TODO: benchmark this against the built-in inverse implementation
# Until then, don't bother with the specialised version.
# function invert_mapping_matrix(A::Matrix{T}) where {T <: AbstractFloat}
#     # The mapping matrix has the following structure:
#     # [ x 0 0 0 0 0 ]
#     # [ 0 x 0 0 0 0 ]
#     # [ 0 0 x 0 0 0 ]
#     # [ x x x x x x ]
#     # [ 0 x x x x x ]
#     # [ 0 0 x x x x ]
#     # ==>
#     # [ A_diag B=0 ]
#     # [ C      D   ]
#     # We make use of the Schur-complement, so the inverse is:
#     # [ inv(A_diag)               0      ]
#     # [ -inv(D) * C * inv(A_diag) inv(D) ]
#     N::Int = LinearAlgebra.checksquare(A)
#     @assert (iseven(N)) "N must be even -- instead, N = $N"
#     n_2::Int = N / 2
#     A_diag = A[1:n_2, 1:n_2]
#     A_diag_inv = inv(A_diag)

#     C = A[(n_2+1):end, 1:n_2]
#     D = A[(n_2+1):end, (n_2+1):end]
#     D_inv = inv(D)

#     Ainv = zeros(T, N, N)
#     Ainv[1:n_2, 1:n_2] = A_diag_inv
#     Ainv[(n_2+1):end, 1:n_2] = -D_inv * C * A_diag_inv
#     Ainv[(n_2+1):end, (n_2+1):end] = D_inv

#     return Ainv
# end

mutable struct Constraint{Dimension, T <: AbstractFloat}
    vertex_index::Int # to which vertex the constraint applies
    derivative_index::Int # derivative corresponding to the constraint, e.g. 1 -> velocity
    constraint::sa.SVector{Dimension, T} # the constraint values
end
Constraint() = Constraint(0, 0, zeros(sa.SVector{3, Float64}))
@inline Base.:(==)(a::Constraint, b::Constraint) = (a.vertex_index == b.vertex_index) &&
                                                   (a.derivative_index == b.derivative_index)
@inline function Base.isless(a::Constraint, b::Constraint)
    if a.vertex_index < b.vertex_index
        return true
    elseif b.vertex_index < a.vertex_index
        return false
    end

    if a.derivative_index < b.derivative_index
        return true
    end
    return false
end

function setup_constraint_reordering_matrix!(prob::PolyOpt{Dimension, T}) where {Dimension, T <: AbstractFloat}
    all_constraints = Vector{Constraint{Dimension, T}}()
    # each segment has constraints for (start & end), from deriviative 0 up to kHighestDerivativeToOptimise
    #    for some reason, Helen's code has: prob.constraint.num_vertices * prob.N / 2 --> TODO
    # each segment has N/2 constraints at each of its ends
    num_constraints = Int(prob.num_segments * prob.N)
    sizehint!(all_constraints, num_constraints)
    fixed_constraints = Vector{Constraint{Dimension, T}}()
    free_constraints = Vector{Constraint{Dimension, T}}()

    # Set up all constraints. Except for the first and last vertices, every vertex acts as a constraint
    # between two segments.
    for (index, vertex) ∈ enumerate(prob.constraint.vertices)
        num_constraint_occurences = (index == 1 || index == prob.constraint.num_vertices) ? 1 : 2
        for co = Base.OneTo(num_constraint_occurences)
            for derivative = 0 : (Int(prob.N / 2) - 1)
                constraint = Constraint(index, derivative, zeros(sa.SVector{Dimension, T}))
                if haskey(vertex, derivative)
                    constraint.constraint = vertex[derivative]
                    push!(all_constraints, constraint)
                    # TODO: use sorted set from DataStructures
                    if constraint ∉ fixed_constraints
                        push!(fixed_constraints, constraint)
                    end
                else
                    push!(all_constraints, constraint)
                    if constraint ∉ free_constraints
                        push!(free_constraints, constraint)
                    end
                end
            end
        end
    end
    # TODO: consider using a Dict(segment, Pair(first_constraint, second_constraint)) & SortedSets

    @assert (length(all_constraints) == num_constraints) "Wrong number of constraints.\n\tExpected $(num_constraints) constraints.\n\tInstead, got $(length(all_constraints))."
    num_constraints = length(all_constraints)
    num_fixed_constraints = length(fixed_constraints)
    num_free_constraints = length(free_constraints)

    prob.constraint.num_constraints = num_constraints
    prob.constraint.num_fixed_constraints = num_fixed_constraints
    prob.constraint.num_free_constraints = num_free_constraints

    prob.C = zeros(T, num_constraints, num_fixed_constraints + num_free_constraints)
    empty!(prob.constraint.df)
    sizehint!(prob.constraint.df, Dimension)
    for dim = Base.OneTo(Dimension)
        push!(prob.constraint.df, zeros(num_fixed_constraints))
    end

    row = 1
    col = 1
    for constraint ∈ all_constraints
        for fixedc ∈ fixed_constraints
            if constraint == fixedc
                prob.C[row, col] = 1.0
                for d = Base.OneTo(Dimension)
                    prob.constraint.df[d][col] = fixedc.constraint[d]
                end
            end
            col +=1
        end
        for freec ∈ free_constraints
            if constraint == freec
                prob.C[row,col] = 1.0
            end
            col += 1
        end
        col = 1
        row += 1
    end

    return all_constraints, fixed_constraints, free_constraints
end

function constructR(prob::PolyOpt{Dimension, T}) where {Dimension, T <: AbstractFloat}
    tmp_size = prob.N * prob.num_segments
    cost_unconstrained = zeros(T, tmp_size, tmp_size)
    for i = Base.OneTo(prob.num_segments)
        Ai = prob.Ainv[i]
        Qi = prob.Q[i]
        H = Ai' * Qi * Ai
        start_pos = (i - 1) * prob.N + 1
        end_pos = start_pos + prob.N - 1
        cost_unconstrained[start_pos:end_pos, start_pos:end_pos] = H
    end
    return prob.C'*cost_unconstrained*prob.C
end

"""
    solveLinear(prob::PolyOpt{Dimension, T}) where {Dimension, T <: AbstractFloat}

Solve the linear, i.e. analytical, [`PolyOpt`](@ref) problem.

Return: a [`PolynomialTrajectory`](@ref) that satisfies the constraints and minimises the user-specified derivative.
"""
function solveLinear(prob::PolyOpt{Dimension, T}) where {Dimension, T <: AbstractFloat}
    R = constructR(prob)

    rpf_row_start = prob.constraint.num_fixed_constraints + 1
    rpf_row_end = rpf_row_start + prob.constraint.num_free_constraints - 1
    rpf_col_start = 1
    rpf_col_end = prob.constraint.num_fixed_constraints
    Rpf = R[rpf_row_start:rpf_row_end, rpf_col_start:rpf_col_end]

    rpp_row_start = rpf_row_start
    rpp_row_end = rpp_row_start + prob.constraint.num_free_constraints - 1
    rpp_col_start = rpf_row_start
    rpp_col_end = rpp_col_start + prob.constraint.num_free_constraints - 1
    Rpp = R[rpp_row_start:rpp_row_end, rpp_col_start:rpp_col_end]
    empty!(prob.constraint.dp)
    sizehint!(prob.constraint.dp, Dimension)
    for dim = Base.OneTo(Dimension)
        push!(prob.constraint.dp, -Rpp \ Rpf * prob.constraint.df[dim])
    end

    trajectory = map_to_coefficients(prob)
    return PolySol(getN(prob), Dimension, prob.derivative_to_optimise, trajectory)
end

function map_to_coefficients(prob::PolyOpt{Dimension, T}) where {Dimension, T <: AbstractFloat}
    trajectories = Vector{PolynomialTrajectory{T}}()
    num_constraints = prob.constraint.num_free_constraints + prob.constraint.num_fixed_constraints
    for segment_i = Base.OneTo(prob.num_segments)
        row_s = (segment_i - 1) * prob.N + 1
        row_e = row_s + prob.N - 1
        col_s = 1
        col_e = col_s + num_constraints - 1
        polynomials = Dict{Dimensions.Dimension, po.Poly{T}}()
        for dim = Base.OneTo(Dimension)
            df = prob.constraint.df[dim]
            dp = prob.constraint.dp[dim]
            d_all = [df; dp]
            new_d = prob.C[row_s:row_e, col_s:col_e] * d_all
            coeffs = prob.Ainv[segment_i] * new_d
            polynomials[dim] = po.Poly(coeffs)
        end
        poly_traj = PolynomialTrajectory{T}(prob.times[segment_i], polynomials)
        push!(trajectories, poly_traj)
    end
    return PiecewiseTrajectory(trajectories)
end
