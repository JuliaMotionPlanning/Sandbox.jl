function isvalid(polyopt::mp.PolyOpt)
    # TODO: turn this into a continuity check and add it to `src`
    return true
end

@testset "set up from vertices" begin
    a = mp.Vertex{3, Float64}()
    get!(a, mp.Derivatives.POSITION, zeros(sa.SVector{3, Float64}))
    b = get(a, mp.Derivatives.POSITION, sa.SVector{3, Float64}(NaN, NaN, NaN))
    @test b[1] == 0.0

    c = mp.Vertex()
    @test mp.get_dimension(c) == 3
end
@testset "PolyConstraint" begin
    # test default constructors for coverage...
    a = mp.PolyConstraint()
    @test length(a.dp) == 0
    @test length(a.df) == 0
    @test length(a.vertices) == 0
    @test true
end
@testset "compute_poly_base_coefficients" begin
    a = mp.compute_poly_base_coefficients(4)
    @test a[1,1] == 1
    @test a[4,4] == 6
    a = mp.compute_poly_base_coefficients(1)
    @test size(a) == (1,1)

    @test_throws DomainError mp.compute_poly_base_coefficients(0)
    @test_throws DomainError mp.compute_poly_base_coefficients(-5)
end
@testset "compute_poly_coefficients" begin
    a = mp.compute_poly_coefficients(4, 1, 1.0)
    @test mp.compute_poly_coefficients(4, 1, 1.0) == [0, 1.0, 2.0, 3.0]
    @test mp.compute_poly_coefficients(4, 0, 0.5) == [1.0, 0.5, 0.25, 0.125]
    @test mp.compute_poly_coefficients(4, 1, 1.0) == mp.compute_poly_coefficients(4, mp.Derivatives.VELOCITY, 1.0)
    @test_throws ArgumentError mp.compute_poly_coefficients(1, 5, 1.0)
    @test_throws DomainError mp.compute_poly_coefficients(10, -1, 1.0)
    @test_throws DomainError mp.compute_poly_coefficients(10, 2, -1.0)
end
@testset "Constraint Equality" begin
    a = mp.Constraint()
    b = mp.Constraint()
    a.vertex_index = 5
    b.vertex_index = 5
    @test a == b
    a.vertex_index = 2
    @test a < b
    a.vertex_index = 6
    @test a > b
    a.vertex_index = 5
    a.derivative_index = 1
    @test a > b
    b.derivative_index = 1
    @test a == b
    a.derivative_index = 0
    @test a < b
end
@testset "Setting up PolyOpt" begin
    dim = 1
    start = mp.Vertex{dim, Float64}()
    start[mp.Derivatives.POSITION] = sa.SVector(1.0)
    start[mp.Derivatives.VELOCITY] = sa.SVector(2.0)
    start[mp.Derivatives.ACCELERATION] = sa.SVector(3.0)
    endpoint = mp.Vertex{dim, Float64}()
    endpoint[mp.Derivatives.POSITION] = sa.SVector(-1.0)
    endpoint[mp.Derivatives.VELOCITY] = sa.SVector(-2.0)
    endpoint[mp.Derivatives.ACCELERATION] = sa.SVector(-3.0)
    vertices = [start, endpoint]
    times = [1.0]
    derivative_to_optimise = 4

    prob = mp.setup(vertices, times, derivative_to_optimise)
    @test mp.getN(prob) == 10
end
@testset "1D trajectory, single segment" begin
    dim = 1
    N = 10
    problem = mp.PolyOpt(dim, N)
    @test mp.getN(problem) == N
    start = mp.Vertex{dim, Float64}()
    start[mp.Derivatives.POSITION] = sa.SVector(1.0)
    start[mp.Derivatives.VELOCITY] = sa.SVector(2.0)
    start[mp.Derivatives.ACCELERATION] = sa.SVector(3.0)
    endpoint = mp.Vertex{dim, Float64}()
    endpoint[mp.Derivatives.POSITION] = sa.SVector(-1.0)
    endpoint[mp.Derivatives.VELOCITY] = sa.SVector(-2.0)
    endpoint[mp.Derivatives.ACCELERATION] = sa.SVector(-3.0)
    vertices = [start, endpoint]
    times = [1.0]
    derivative_to_optimise = 4
    newN = 12
    mp.setN!(problem, newN)
    @test mp.getN(problem) == newN
    @test mp.get_highest_derivative_to_optimise(problem) == newN / 2 - 1

    mp.setup!(problem, vertices, times, derivative_to_optimise)
    @test mp.getN(problem) == newN
    sol = mp.solveLinear(problem)
    traj = sol.trajectory

    first_sp = mp.get_first(traj)
    @test mp.position(first_sp) ≈ [1.0]
    @test mp.velocity(first_sp) ≈ [2.0]
    @test mp.acceleration(first_sp) ≈ [3.0]
    last_sp = mp.get_last(traj)
    @test mp.position(last_sp) ≈ [-1.0]
    @test mp.velocity(last_sp) ≈ [-2.0]
    @test mp.acceleration(last_sp) ≈ [-3.0]
end
@testset "3D trajectory, single segment" begin
    dim = 3
    N = 10
    problem = mp.PolyOpt(dim, N)
    @test problem.N == N
    start = mp.Vertex{dim, Float64}()
    start[mp.Derivatives.POSITION] = sa.SVector(1,1,1)
    start[mp.Derivatives.VELOCITY] = sa.SVector(2,2,2)
    start[mp.Derivatives.ACCELERATION] = sa.SVector(3,3,3)
    endpoint = mp.Vertex{dim, Float64}()
    endpoint[mp.Derivatives.POSITION] = sa.SVector(-1,0,0)
    endpoint[mp.Derivatives.VELOCITY] = sa.SVector(-2,0,0)
    endpoint[mp.Derivatives.ACCELERATION] = sa.SVector(-3,0,0)
    vertices = [start, endpoint]
    times = [1.0]
    derivative_to_optimise = 4
    mp.setup!(problem, vertices, times, derivative_to_optimise)
    sol = mp.solveLinear(problem)
    traj = sol.trajectory

    first_sp = mp.get_first(traj)
    @test mp.position(first_sp) ≈ [1.0, 1, 1]
    @test mp.velocity(first_sp) ≈ [2.0, 2, 2]
    @test mp.acceleration(first_sp) ≈ [3.0, 3, 3]
    last_sp = mp.get_last(traj)
    @test mp.position(last_sp) ≈ [-1.0, 0, 0]
    @test mp.velocity(last_sp) ≈ [-2.0, 0, 0]
    @test mp.acceleration(last_sp) ≈ [-3.0, 0, 0]
end
@testset "1D trajectory, double segment" begin
    dim = 1
    N = 10
    problem = mp.PolyOpt(dim, N)
    @test problem.N == N
    start = mp.Vertex{dim, Float64}()
    start[mp.Derivatives.POSITION] = sa.SVector(1.0)
    start[mp.Derivatives.VELOCITY] = sa.SVector(2.0)
    start[mp.Derivatives.ACCELERATION] = sa.SVector(3.0)
    midpoint = mp.Vertex{dim, Float64}()
    midpoint[mp.Derivatives.POSITION] = sa.SVector(-1.0)
    midpoint[mp.Derivatives.VELOCITY] = sa.SVector(-2.0)
    midpoint[mp.Derivatives.ACCELERATION] = sa.SVector(-3.0)
    endpoint = mp.Vertex{dim, Float64}()
    endpoint[mp.Derivatives.POSITION] = sa.SVector(-1.0)
    endpoint[mp.Derivatives.VELOCITY] = sa.SVector(-2.0)
    endpoint[mp.Derivatives.ACCELERATION] = sa.SVector(-3.0)
    vertices = [start, midpoint, endpoint]
    times = [1.0, 1.0]
    derivative_to_optimise = 4
    mp.setup!(problem, vertices, times, derivative_to_optimise)
    sol = mp.solveLinear(problem)
    traj = sol.trajectory

    first_sp = mp.get_first(traj)
    @test mp.position(first_sp) ≈ [1.0]
    @test mp.velocity(first_sp) ≈ [2.0]
    @test mp.acceleration(first_sp) ≈ [3.0]
    mid_sp = mp.evaluate(traj, 1)
    @test mp.position(mid_sp) ≈ [-1.0]
    @test mp.velocity(mid_sp) ≈ [-2.0]
    @test mp.acceleration(mid_sp) ≈ [-3.0]
    last_sp = mp.get_last(traj)
    @test mp.position(last_sp) ≈ [-1.0]
    @test mp.velocity(last_sp) ≈ [-2.0]
    @test mp.acceleration(last_sp) ≈ [-3.0]
end