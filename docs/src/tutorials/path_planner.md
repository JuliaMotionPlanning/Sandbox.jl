```@example path_planning
using MotionPlanning # hide
const mp = MotionPlanning # hide
using FileIO # hide
```

# Path Planning

## Setup: load the map and set start + destination

```@example path_planning
img_path = joinpath(@__DIR__, "../../../test/maps/simple_map.png")
gm = mp.image2gridmap(img_path, mp.Point2D(0,0), 0.05)
y = mp.getheightmetric(gm)
s = mp.Point2D(0, 0)
d = mp.Point2D(0, y)
nothing # hide
```

## BFS

```@example path_planning
bfs_cell_path, bfs_visited_cells = mp.bfs(gm, s, d)
bfs_path = mp.indices2position(gm, bfs_cell_path)
bfs_img = mp.to_image(gm, bfs_path)
save("bfs_img.png", bfs_img); nothing # hide
```
![bfs_img](bfs_img.png)

## Dijkstra

```@example path_planning
dijkstra_cell_path, dijkstra_visited_cells = mp.dijkstra(gm, s, d)
dijkstra_path = mp.indices2position(gm, dijkstra_cell_path)
dijkstra_img = mp.to_image(gm, dijkstra_path)
save("dijkstra_img.png", dijkstra_img); nothing # hide
```
![dijkstra_img](dijkstra_img.png)

## A*

```@example path_planning
astar_cell_path, astar_visited_cells = mp.astar(gm, s, d)
astar_path = mp.indices2position(gm, astar_cell_path)
astar_img = mp.to_image(gm, astar_path)
save("astar_img.png", astar_img); nothing # hide
```
![astar_img](astar_img.png)

## Compare results
First, let's compare the number of cells:

```@example path_planning
println("BFS length: $(length(bfs_cell_path.indices))")
println("Dijkstra length: $(length(dijkstra_cell_path.indices))")
println("A* length: $(length(astar_cell_path.indices))")
```

Next, let's compare the metric distance of all paths:

```@example path_planning
println("BFS length: $(mp.arclength(bfs_path))")
println("Dijkstra length: $(mp.arclength(dijkstra_path))")
println("A* length: $(mp.arclength(astar_path))")
```

Finally, let's check the number of nodes that were visited.

```@example path_planning
println("BFS visited: $(length(bfs_visited_cells))")
println("Dijkstra visited: $(length(dijkstra_visited_cells))")
println("A* visited: $(length(astar_visited_cells))")
```

Because of the obstacle's location, the number of visisted nodes isn't greatly reduced.
If we removed the obstacle, we would see that A* visits drastically fewer nodes.

```@example path_planning
img_path = joinpath(@__DIR__, "../../../test/maps/free_map_small.png")
gm = mp.image2gridmap(img_path, mp.Point2D(0,0), 0.05)

bfs_cell_path, bfs_visited_cells = mp.bfs(gm, s, d)
dijkstra_cell_path, dijkstra_visited_cells = mp.dijkstra(gm, s, d)
astar_cell_path, astar_visited_cells = mp.astar(gm, s, d)

println("BFS visited: $(length(bfs_visited_cells))")
println("Dijkstra visited: $(length(dijkstra_visited_cells))")
println("A* visited: $(length(astar_visited_cells))")
```
