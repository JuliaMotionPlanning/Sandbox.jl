#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#
"""
    PiecewiseTrajectory(trajectories)

trajectories: vector of AbstractTrajectory

*WARNING:* had trouble with abstract trajectories, so made it a vector of PolynomialTrajectory's instead.
"""
mutable struct PiecewiseTrajectory{T <: AbstractFloat} <: AbstractTrajectory{T}
    trajectories::Vector{PolynomialTrajectory{T}}
    function PiecewiseTrajectory{T}(trajectories::Vector{PolynomialTrajectory{T}}) where T
        @assert !isempty(trajectories) "[PiecewiseTrajectory] trajectorys cannot be empty."
        if length(trajectories) > 1
            new_trajectories = trajectories
            new_trajectories[1] = copy(trajectories[1])
            for i = 2:length(new_trajectories)
                new_traj = copy(trajectories[i])
                set_start_time!(new_traj, get_end_time(new_trajectories[i - 1]))
                new_trajectories[i] = new_traj
            end
            return new{T}(new_trajectories)
        else
            return new{T}(trajectories)
        end
    end
end

function Base.show(io::IO, ::MIME"text/plain", traj::PiecewiseTrajectory)
    print(io, traj)
end

function Base.show(io::IO, traj::PiecewiseTrajectory)
    str = "[Piecewise Trajectory]\n" *
          "\tDimension: $(num_dimensions(traj))\n" *
          "\tNumber of trajectories: $(length(traj.trajectories))\n" *
          "\tTime (start, stop, duration): ($(get_start_time(traj)), $(get_end_time(traj)), $(get_duration(traj)))\n" *
          "\tTrajectories: "
    print(io, str, traj.trajectories)
end

PiecewiseTrajectory(trajectories::Vector{PolynomialTrajectory{T}}) where T = PiecewiseTrajectory{T}(trajectories)
PiecewiseTrajectory(traj::PiecewiseTrajectory{T}) where T = PiecewiseTrajectory(traj.trajectories)
function PiecewiseTrajectory(traj0::PiecewiseTrajectory{T}, traj1::PiecewiseTrajectory{T}) where T
    return PiecewiseTrajectory([traj0.trajectories, traj1.trajectories])
end
function PiecewiseTrajectory(trajectories::Vector{PiecewiseTrajectory{T}}) where T
    traj_vec = Vector{PolynomialTrajectory{T}}()
    for traj ∈ trajectories
        traj_vec = vcat(traj_vec, traj.trajectories)
    end
    return PiecewiseTrajectory(traj_vec)
end

get_start_time(traj::PiecewiseTrajectory) = get_start_time(traj.trajectories[1])
get_end_time(traj::PiecewiseTrajectory) = get_end_time(traj.trajectories[end])
num_dimensions(traj::PiecewiseTrajectory) = minimum([num_dimensions(trajectory) for trajectory ∈ traj.trajectories])
num_derivatives(traj::PiecewiseTrajectory) = minimum([num_derivatives(trajectory) for trajectory ∈ traj.trajectories])
getN(traj::PiecewiseTrajectory) = getN(traj.trajectories[1])

function set_start_time!(trajectory::PiecewiseTrajectory, new_time::Real)
    dt = new_time - get_start_time(trajectory)
    for traj in trajectory.trajectories
        set_start_time!(traj, get_start_time(traj) + dt)
    end
end

function copy(trajectory::PiecewiseTrajectory{T}) where {T <: AbstractFloat}
    new_traj = Vector{PolynomialTrajectory{T}}(undef, length(trajectory.trajectories))
    for i = 1:length(trajectory.trajectories)
        new_traj[i] = copy(trajectory.trajectories[i])
    end
    return PiecewiseTrajectory(new_traj)
end

function evaluate(traj::PiecewiseTrajectory, time::Real)
    if time < get_start_time(traj)
        return evaluate(traj.trajectories[1], time)
    elseif time >= get_end_time(traj)
        return evaluate(traj.trajectories[end], time)
    end

    for single_traj in traj.trajectories
        if (get_start_time(single_traj) <= time) && (get_end_time(single_traj) > time)
            return evaluate(single_traj, time)
        end
    end
    error("Should not get here.")
end

function derivative(traj::PiecewiseTrajectory{T}) where {T <: AbstractFloat}
    new_traj = Vector{PolynomialTrajectory{T}}()
    for polytraj ∈ traj.trajectories
        push!(new_traj, derivative(polytraj))
    end
    return PiecewiseTrajectory{T}(new_traj)
end
