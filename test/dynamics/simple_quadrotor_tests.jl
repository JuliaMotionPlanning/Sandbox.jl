@testset "Simple quadrotor, fake motor model" begin
    @testset "Constant force, zero moment" begin
        mass = 2.0
        Jxx = 2.0; Jyy = 2.0; Jzz = 2.0;
        J = sa.SA_F64[Jxx 0 0;
               0 Jyy 0;
               0 0 Jzz]
        Fx = 1; Fy = 0; Fz = 0;
        F = sa.SA_F64[Fx, Fy, Fz]

        Mx = 0; My = 0; Mz = 0;
        M = sa.SA_F64[Mx, My, Mz]

        rb = mp.RigidBodyFixedCoM(mass, J, zeros(SVector{3}), zeros(SVector{3}))
        mm = mp.FakeMotorModel(F, M)

        sq = mp.SimpleQuadrotor(rb, mm)

        xₒ = [2, -1, 0.5]
        vₒ = [-.1, 0, 0.0]
        qₒ = [1.0, 0, 0, 0.0]
        ωₒ = [-.2, 0.0, 0.0]
        uₒ = [xₒ; vₒ; qₒ; ωₒ; 0.0]

        tf = 5.0
        tspan = (0.0, tf)
        prob_quat = ODEProblem(mp.simulate, uₒ, tspan, sq)
        sol_quat = solve(prob_quat, Tsit5())

        accel = [Fx, Fy, Fz] / mass + [0, 0, am.g]
        pos(t) = 0.5 * accel * t^2 + vₒ * t + xₒ
        vel(t) = accel * t + vₒ
        @assert isdiag(J)
        α = J \ M
        rot(t) = 0.5 * α * t^2 + ωₒ * t
        ω(t) = α * t + ωₒ

        ϵ = 1e-8
        for (t, u) ∈ zip(sol_quat.t, sol_quat.u)
            sim_pos = u[mp.getPositionInd(rb)]
            expected_pos = pos(t)
            @test isapprox(norm(expected_pos - sim_pos), 0.0, atol = ϵ)

            sim_vel = u[mp.getVelocityInd(rb)]
            expected_vel = vel(t)
            @test isapprox(norm(expected_vel - sim_vel), 0.0, atol = ϵ)

            qf_tmp = u[mp.getQuaternionInd(rb)]
            qf = Quat(qf_tmp...)
            r_xyz_quat = RotXYZ(qf)
            rpy = [r_xyz_quat.theta1, r_xyz_quat.theta2, r_xyz_quat.theta3]
            @test isapprox(norm(rpy - rot(t)), 0.0, atol = 1e3 * ϵ)

            ωsim = u[mp.getAngularVelocityInd(rb)]
            @test isapprox(norm(ωsim - ω(t)), 0.0, atol = ϵ)
        end
    end
    @testset "Constant moment, zero force" begin
        mass = 2.0
        Jxx = 2.0; Jyy = 2.0; Jzz = 2.0;
        J = sa.SA_F64[Jxx 0 0;
               0 Jyy 0;
               0 0 Jzz]
        Fx = 0; Fy = 0; Fz = 0;
        F = sa.SA_F64[Fx, Fy, Fz]

        Mx = 0; My = 0.25; Mz = 0;
        M = sa.SA_F64[Mx, My, Mz]

        rb = mp.RigidBodyFixedCoM(mass, J, zeros(SVector{3}), zeros(SVector{3}))
        mm = mp.FakeMotorModel(F, M)

        sq = mp.SimpleQuadrotor(rb, mm)

        xₒ = [2, -1, 0.5]
        vₒ = [-.1, 0, 0.0]
        qₒ = [1.0, 0, 0, 0.0]
        ωₒ = [0.0, 0.0, 0.0]
        uₒ = [xₒ; vₒ; qₒ; ωₒ; 0.0]

        tf = 5.0
        tspan = (0.0, tf)
        prob_quat = ODEProblem(mp.simulate, uₒ, tspan, sq)
        sol_quat = solve(prob_quat, Tsit5(), dtmax = 0.1)

        accel = [Fx, Fy, Fz] / mass + [0, 0, am.g]
        pos(t) = 0.5 * accel * t^2 + vₒ * t + xₒ
        vel(t) = accel * t + vₒ
        @assert isdiag(J)
        α = J \ M
        rot(t) = 0.5 * α * t^2 + ωₒ * t
        ω(t) = α * t + ωₒ

        ϵ = 1e-8
        for (t, u) ∈ zip(sol_quat.t, sol_quat.u)
            sim_pos = u[mp.getPositionInd(rb)]
            expected_pos = pos(t)
            @test isapprox(norm(expected_pos - sim_pos), 0.0, atol = ϵ)

            sim_vel = u[mp.getVelocityInd(rb)]
            expected_vel = vel(t)
            @test isapprox(norm(expected_vel - sim_vel), 0.0, atol = ϵ)

            qf_tmp = u[mp.getQuaternionInd(rb)]
            qf = Quat(qf_tmp...)
            r_xyz_quat = RotXYZ(qf)
            rpy = [r_xyz_quat.theta1, r_xyz_quat.theta2, r_xyz_quat.theta3]
            @test isapprox(norm(rpy - rot(t)), 0.0, atol = ϵ)

            ωsim = u[mp.getAngularVelocityInd(rb)]
            @test isapprox(norm(ωsim - ω(t)), 0.0, atol = ϵ)
        end
    end
end

@testset "Simple quadrotor with 'X' motor model" begin
    @testset "Approximate hover thrust" begin
        # Motor model
        τ = 0.05
        k_f = 3.32136e-8
        k_m = 8.1834e-10
        l = 0.14
        motor_params = mp.XMotorModelParams(τ, k_f, k_m, l)
        motor = mp.XMotorModel(motor_params)
        ω_hover = 12253.385290218743
        motor.ωr = [ω_hover, ω_hover, ω_hover, ω_hover]

        # Rigid Body parameters
        m = 2.0
        J = SA_F64[2.0 0 0;
            0 2.0 0;
            0 0 2.0]
        F = SA_F64[1,1,1]
        M = SA_F64[5,0,0]
        rb = mp.RigidBodyFixedCoM(m, J, F, M)

        # Initial conditions
        xₒ = [0, 0, 0.0]
        vₒ = [0, 0, 0.0]
        init_yaw = deg2rad(45)
        init_rot = RotXYZ(0, 0, init_yaw)
        qinit = Quat(init_rot)
        qₒ = [qinit.w, qinit.x, qinit.y, qinit.z]
        ωₒ = [0, 0, 0.0]
        uₒ = [xₒ; vₒ; qₒ; ωₒ]

        # Create quadrotor
        myquad = mp.SimpleQuadrotor(rb, motor)

        # Create & solve the ODE
        u0 = zeros(mp.sizeStateVector(myquad))
        u0[1:mp.sizeStateVector(rb)] = uₒ
        tspan = (0.0, 2.0)
        prob = ODEProblem(mp.simulate, u0, tspan, myquad)
        sol = solve(prob, Tsit5())

        # Check test conditions
        sim_pos = sol[mp.getPositionInd(myquad), :]
        @test all(x->(x ≈ 0), sim_pos[1, :])
        @test all(y->(y ≈ 0), sim_pos[2, :])
        # hover thrust isn't exact, so there will be some drift in Z over the 2s span
        @test all(z->(z < 1.2), sim_pos[3, :])

        sim_vel = sol[mp.getVelocityInd(myquad), :]
        @test all(vx->(vx ≈ 0), sim_vel[1, :])
        @test all(vy->(vy ≈ 0), sim_vel[2, :])
        # hover thrust isn't exact, so there will be some drift in Z over the 2s span
        @test all(vz->(vz < 0.75), sim_vel[3, :])

        qf_tmp = sol[mp.getQuaternionInd(myquad), :]
        qf = [Quat(q_col...) for q_col ∈ eachcol(qf_tmp)]
        r_xyz_quat = [RotXYZ(q) for q ∈ qf]
        @test all(r->r.theta1 ≈ 0, r_xyz_quat)
        @test all(r->r.theta2 ≈ 0, r_xyz_quat)
        @test all(r->r.theta3 ≈ init_yaw, r_xyz_quat)

        ωsim = sol[mp.getAngularVelocityInd(myquad), :]
        @test all(ωx->(ωx ≈ 0), ωsim[1, :])
        @test all(ωy->(ωy ≈ 0), ωsim[2, :])
        @test all(ωy->(ωy ≈ 0), ωsim[3, :])
    end
end

@testset "Simple quadrotor utilities" begin
    mp.getPositionInd(mp.SimpleQuadrotor)
    mp.getVelocityInd(mp.SimpleQuadrotor)
    mp.getQuaternionInd(mp.SimpleQuadrotor)
    mp.getAngularVelocityInd(mp.SimpleQuadrotor)
end