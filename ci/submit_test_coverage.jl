
include("init.jl")

cd(Pkg.dir("MotionPlanning"))

Pkg.add("Coverage")
using Coverage
Codecov.submit_local(process_folder())
