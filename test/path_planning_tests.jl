@testset "On-map" begin
    gm = mp.Gridmap2D()
    gm.data = rand(UInt8, 2, 4)
    gm.resolution = 1.0
    gm.origin = mp.Point2D(0, 0)

    p0 = mp.Point2D(5, 5)
    @test !mp.on_map(gm, p0)

    p1 = mp.Point2D(0, 0)
    @test mp.on_map(gm, p1)

    p2 = mp.Point2D(-1, -1)
    @test !mp.on_map(gm, p2)
end

@testset "Get Index from Point" begin
    gm = mp.Gridmap2D()
    gm.data = rand(UInt8, 2, 4)
    gm.resolution = 1.0
    gm.origin = mp.Point2D(0, 0)

    a = mp.Point2D(0, 0)
    ind = mp.get_linear_index(gm, a)
    @test ind == 1

    a.x = 2
    ind = mp.get_linear_index(gm, a)
    @test ind == 2

    a.x = 0
    a.y = 2
    ind = mp.get_linear_index(gm, a)
    @test ind == 5

    a.x = 2
    a.y = 4
    ind = mp.get_linear_index(gm, a)
    @test ind == 8

    a.x = 50
    a.y = 50
    @test mp.get_linear_index(gm, a) == nothing
end

@testset "Converting to/from indices & positions" begin
    gm = mp.Gridmap2D()
    gm.data = rand(UInt8, 2, 4)
    gm.resolution = 1.0
    gm.origin = mp.Point2D(0, 0)

    point = mp.Point2D(1.0, 1.0)
    ind = mp.get_cartesian_index(gm, point)
    @test ind == CartesianIndex(2, 2)
    new_point = mp.get_position(gm, ind)
    @test new_point == mp.Point2D(1.5, 1.5)

    point = mp.Point2D(50, 50)
    ind = mp.get_cartesian_index(gm, point)
    @test ind == nothing
end

@testset "Get neighbours (four)" begin
    gm = mp.Gridmap2D()
    gm.data = zeros(UInt8, 10, 10)
    gm.resolution = 0.05
    gm.origin = mp.Point2D(0, 0)

    a = CartesianIndex(1, 1)
    neighbours = mp.get_neighbours_4(gm, a)
    @test length(neighbours) == 2
    @test CartesianIndex(1, 2) ∈ neighbours
    @test CartesianIndex(2, 1) ∈ neighbours
    @test mp.has_left(gm, a) == false
    @test mp.has_right(gm, a) == true
    @test mp.has_up(gm, a) == false
    @test mp.has_down(gm, a) == true

    a = CartesianIndex(5, 5)
    neighbours = mp.get_neighbours_4(gm, a)
    @test length(neighbours) == 4
    @test CartesianIndex(5, 6) ∈ neighbours
    @test CartesianIndex(5, 4) ∈ neighbours
    @test CartesianIndex(6, 5) ∈ neighbours
    @test CartesianIndex(4, 5) ∈ neighbours
    @test mp.has_left(gm, a) == true
    @test mp.has_right(gm, a) == true
    @test mp.has_up(gm, a) == true
    @test mp.has_down(gm, a) == true

    a = CartesianIndex(1, 10)
    neighbours = mp.get_neighbours_4(gm, a)
    @test length(neighbours) == 2
    @test CartesianIndex(1, 9) ∈ neighbours
    @test CartesianIndex(2, 10) ∈ neighbours
    @test mp.has_left(gm, a) == false
    @test mp.has_right(gm, a) == true
    @test mp.has_up(gm, a) == true
    @test mp.has_down(gm, a) == false

    a = CartesianIndex(10, 1)
    neighbours = mp.get_neighbours_4(gm, a)
    @test length(neighbours) == 2
    @test CartesianIndex(10, 2) ∈ neighbours
    @test CartesianIndex(9, 1) ∈ neighbours
    @test mp.has_left(gm, a) == true
    @test mp.has_right(gm, a) == false
    @test mp.has_up(gm, a) == false
    @test mp.has_down(gm, a) == true

    a = CartesianIndex(0, 1)
    @test_throws BoundsError mp.get_neighbours_4(gm, a)
    a = CartesianIndex(1, 0)
    @test_throws BoundsError mp.get_neighbours_4(gm, a)
    a = CartesianIndex(1, 11)
    @test_throws BoundsError mp.get_neighbours_4(gm, a)
    a = CartesianIndex(11, 1)
    @test_throws BoundsError mp.get_neighbours_4(gm, a)
end

@testset "Safety checks on free map" begin
    gm = mp.Gridmap2D()
    gm.data = zeros(UInt8, 100, 100)
    gm.resolution = 0.05
    gm.origin = mp.Point2D(0, 0)

    @test mp.is_safe(gm, CartesianIndex(1, 1))
    @test mp.is_safe_or_unknown(gm, CartesianIndex(1, 1))

    gm.data[10, 10] = mp.Mapping.LETHAL
    @test !mp.is_safe(gm, CartesianIndex(10, 10))
    @test !mp.is_safe_or_unknown(gm, CartesianIndex(10, 10))

    gm.data[20, 20] = mp.Mapping.NO_INFORMATION
    @test !mp.is_safe(gm, CartesianIndex(20, 20))
    @test mp.is_safe_or_unknown(gm, CartesianIndex(20, 20))
end

@testset "BFS on free map" begin
    gm = mp.Gridmap2D()
    gm.data = zeros(UInt8, 100, 100)
    res = 0.05
    gm.resolution = res
    gm.origin = mp.Point2D(0, 0)

    # If we go straight to the right, then we expect a straight path
    s = mp.Point2D(res, res)
    d = mp.Point2D(5 * res, res)
    path, _ = mp.bfs(gm, s, d)
    @test !isempty(path.indices)
    @test length(path.indices) == 5
    @test path.indices[1] == mp.get_cartesian_index(gm, s)
    @test path.indices[end] == mp.get_cartesian_index(gm, d)
    path_meters = mp.indices2position(gm, path)
    for (index, point) ∈ enumerate(path_meters.points)
        y = 1.5 * res
        x = index * res + res / 2
        @test point == mp.Point2D(x, y)
    end

    # Move the destination off the gridmap -- we can't plan to it, so expect `nothing`
    d.x = 10
    @test_throws BoundsError mp.bfs(gm, s, d)
    # Move the start off the gridmap -- expect `nothing`
    s.x = 10
    d.x = 2
    @test_throws BoundsError mp.bfs(gm, s, d)
end

@testset "Search on simple_map" begin
    gm = mp.image2gridmap("simple_map", mp.Point2D(0, 0), 0.05, smart_load = true)
    y = mp.getheightmetric(gm)
    s = mp.Point2D(0, 0)
    d = mp.Point2D(0, y)

    bfs_cell_path, bfs_prev = mp.bfs(gm, s, d)
    bfs_path = mp.indices2position(gm, bfs_cell_path)
    @test !isempty(bfs_path.points)
    @test bfs_cell_path.indices[end] == mp.get_cartesian_index(gm, d)
    @test bfs_cell_path.indices[1] == mp.get_cartesian_index(gm, s)
    bfs_img = mp.to_image(gm, bfs_path)
    # home = ENV["HOME"]
    # save(joinpath(home, "/Downloads/simple_map_bfs.png"), bfs_img)

    dijkstra_cell_path, dijkstra_prev = mp.dijkstra(gm, s, d)
    dijkstra_path = mp.indices2position(gm, dijkstra_cell_path)
    @test !isempty(dijkstra_path.points)
    @test dijkstra_cell_path.indices[end] == mp.get_cartesian_index(gm, d)
    @test dijkstra_cell_path.indices[1] == mp.get_cartesian_index(gm, s)
    dijkstra_img = mp.to_image(gm, dijkstra_path)
    # save(joinpath(home, "/Downloads/simple_map_dijkstra.png"), dijkstra_img)

    astar_cell_path, astar_prev = mp.astar(gm, s, d)
    astar_path = mp.indices2position(gm, astar_cell_path)
    @test !isempty(astar_path.points)
    @test astar_cell_path.indices[end] == mp.get_cartesian_index(gm, d)
    @test astar_cell_path.indices[1] == mp.get_cartesian_index(gm, s)
    astar_img = mp.to_image(gm, astar_path)
    # save(joinpath(home, "/Downloads/simple_map_astar.png"), astar_img)

    @test length(bfs_cell_path.indices) == length(dijkstra_cell_path.indices) == length(astar_cell_path.indices)
    @test abs(mp.arclength(bfs_path) - mp.arclength(dijkstra_path)) < 1e-4
    @test abs(mp.arclength(bfs_path) - mp.arclength(astar_path)) < 1e-4

    d.y = 5*y
    @test_throws BoundsError mp.dijkstra(gm, s, d)
    d.y = y
    s.y = 5*y
    @test_throws BoundsError mp.dijkstra(gm, s, d)
end

@testset "Search on lethal map" begin
    # Starting from the bottom-left, try to go to the top-right.
    # The map is basically entirely lethal, so all searches should fail and return `nothing`.
    gm = mp.image2gridmap("lethal_map_small", mp.Point2D(0, 0), 0.05, smart_load = true)
    y = mp.getheightmetric(gm)
    s = mp.Point2D(0, 0)
    d = mp.Point2D(0, y)

    bfs_cell_path, bfs_prev = mp.bfs(gm, s, d)
    @test bfs_cell_path == nothing

    dijkstra_cell_path, dijkstra_prev = mp.dijkstra(gm, s, d)
    @test dijkstra_cell_path == nothing

    astar_cell_path, astar_prev = mp.astar(gm, s, d)
    @test astar_cell_path == nothing
end