using MotionPlanning
const mp = MotionPlanning

# # Polynomial Optimisation
# First, set up basic parameters:
# - `dim`: the dimension of the problem. In this case, it is 2, as we will be planning in XY.
# - `N`: the number of polynomial coefficients. By setting it to 12, we are able to get snap-continuous trajectories.
dim = 2
N = 12

poly_opt = mp.PolyOpt(dim, N)

# Next, we need to set the boundary constraints between each of our segments.
# We will have 2 segments, or 3 vertices. By not specificying a constraint
# for a particular derivative, you are leaving it free. E.g. `start` only has
# a constraint on position; `midpoint` on position and velocity; `goal` on
# position, velocity, and acceleration.
start = mp.Vertex{dim, Float64}()
start[mp.Derivatives.POSITION] = [1, 1]
start[mp.Derivatives.VELOCITY] = [0, 0]
start[mp.Derivatives.ACCELERATION] = [0, 0]
midpoint1 = mp.Vertex{dim, Float64}()
midpoint1[mp.Derivatives.POSITION] = [2, 0.7]
midpoint1[mp.Derivatives.VELOCITY] = [1, 1]
midpoint2 = mp.Vertex{dim, Float64}()
midpoint2[mp.Derivatives.POSITION] = [3, 0.7]
midpoint3 = mp.Vertex{dim, Float64}()
midpoint3[mp.Derivatives.POSITION] = [4, 0.7]
goal = mp.Vertex{dim, Float64}()
goal[mp.Derivatives.POSITION] = [3, -1]
goal[mp.Derivatives.VELOCITY] = [0, 0]
goal[mp.Derivatives.ACCELERATION] = [0, 0]

vertices = [start, midpoint1, midpoint2, midpoint3, goal]

# You must also specify how much time it takes to get from one vertex to the next.
# For this example, we will keep it simple:
times = ones(length(vertices) - 1)

# Choose the derivative you want to optimise
derivative_to_optimise = mp.Derivatives.SNAP

# Finish setting up the problem and solve!
mp.setup!(poly_opt, vertices, times, derivative_to_optimise)
sol = mp.solveLinear(poly_opt)

# You can plot the trajectory too.
mp.polyplot(sol)
