# MotionPlanning
The MotionPlanning package provides useful datatypes and algorithms for:
* global planning on gridmaps
* trajectory generation

The package repository is <https://gitlab.com/kylecarbon/MotionPlanning.jl>.

## Quickstart
```
using Pkg
Pkg.add("https://gitlab.com/kylecarbon/MotionPlanning.jl")
using MotionPlanning
```

## Tutorials
```@contents
Pages = ["tutorials/poly_opt.md",
         "tutorials/path_planner.md"]
Depth = 2
```

## APIs
### Datatypes
```@contents
Pages = ["pages/datatypes.md"]
Depth = 2
```

### Path Planning
```@contents
Pages = ["pages/path_planning.md]
Depth = 2
```

### Polynomial Optimisation
```@contents
Pages = ["pages/polynomial_optimisation.md]
Depth = 2
```

### Utilities
```@contents
Pages = ["pages/utilities.md"]
Depth = 2
```
