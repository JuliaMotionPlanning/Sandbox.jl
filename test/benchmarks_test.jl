@testset "PolyOpt Benchmarks & Plot Utils" begin
    dim = 5
    num_coefficients = 12
    results = mp.polyopt_benchmark_segment(max_segments = 10, dim = dim, num_coefficients = num_coefficients, samples = 1)
    p = mp.plot_benchmark(results, test_name = "segment",
                          benchmark_field = :time,
                          title = "Solve time vs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                          xaxis_title = "Number of segments",
                          yaxis_title = "Median solve time [μs]")
    p = mp.plot_benchmark(results, test_name = "segment",
                          benchmark_field = :memory,
                          title = "Memory allocated vs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                          xaxis_title = "Number of segments",
                          yaxis_title = "Median memory allocated [MiB]")
    p = mp.plot_benchmark(results, test_name = "segment",
                          benchmark_field = :allocs,
                          title = "Number of allocationsvs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                          xaxis_title = "Number of segments",
                          yaxis_title = "Allocations [count]")
    @test_throws ArgumentError mp.plot_benchmark(results, test_name = "segment",
                                   benchmark_field = :garbage_symbol_should_throw,
                                   title = "Number of allocationsvs. number of segments for Dimension = $dim, N = $(num_coefficients)",
                                   xaxis_title = "Number of segments",
                                   yaxis_title = "Allocations [count]")
    @test true
end

@testset "Path planning benchmarks" begin
    results = mp.gridmap_search_benchmark(samples = 1)
    @test true
end