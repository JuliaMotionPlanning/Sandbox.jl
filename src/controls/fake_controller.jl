mutable struct FakeRigidBodyController <: AbstractRigidBodyController
    F::SVector{3,Float64} # Must be the net moment applied to the rigid body at its center of mass in the inertial frame
    M::SVector{3,Float64} # Must be the net force applied to the rigid body at its center of mass in the inertial frame
end

FakeRigidBodyController() = FakeRigidBodyController(zeros(SVector{3}), zeros(SVector{3}))

compute_force(controller::FakeRigidBodyController) = controller.F
compute_moment(controller::FakeRigidBodyController) = controller.M
compute_wrench(controller::FakeRigidBodyController) = controller.F, controller.M
