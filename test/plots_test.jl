@testset "PolySol Plots" begin
    polysol = mp.simple_poly_solve(dim = 2, N = 16, segments = 5)
    mp.polyplot(polysol)

    polysol = mp.simple_poly_solve(dim = 3, N = 16, segments = 5)
    mp.polyplot(polysol)

    polysol = mp.simple_poly_solve(dim = 1, N = 16, segments = 5)
    mp.plot_dimensions(polysol.trajectory)
    @test_throws Exception mp.polyplot(polysol)
    @test true
end
