include("init.jl")

Pkg.test("MotionPlanning"; coverage=true)

Pkg.add("Coverage")
Pkg.update()

cd(Pkg.dir("MotionPlanning"))

using Coverage, MotionPlanning
coverage = process_folder()
covered_lines, total_lines = get_summary(coverage)
percentage = covered_lines / total_lines * 100.0
println("($(percentage)%) covered")
if isinteractive()
    clean_folder(MotionPlanning.mpdir())
end
