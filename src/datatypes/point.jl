#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    PointND
    PointND{N, T <: AbstractFloat} <: StaticArrays.FieldVector{N, T}

An abstract type to represent an N-dimensional point.
"""
abstract type PointND{N, T <: AbstractFloat} <: sa.FieldVector{N, T} end

"""
    Point2D{T <: AbstractFloat} <: PointND{2, T}

Create a point in 2D space, with components (x, y).
"""
mutable struct Point2D{T <: AbstractFloat} <: PointND{2, T}
    x::T
    y::T
end

Point2D() = Point2D(NaN, NaN)
Point2D(x::AbstractFloat) = Point2D(x, x)
Point2D(x::Integer) = Point2D(Float64(x))
Point2D(x::sa.SArray{Tuple{2},T,1,2}) where T = Point2D(x[1], x[2])
Point2D(x::Real, y::Real) = Point2D{Float64}(x, y)

@inline Base.:+(a::Point2D, b::Point2D) = Point2D(broadcast(Base.:+, a, b))
@inline Base.:-(a::Point2D, b::Point2D) = Point2D(broadcast(Base.:-, a, b))
@inline Base.:*(a::Point2D, b::Number) = Point2D(broadcast(Base.:*, b, a))
@inline Base.:*(a::Number, b::Point2D) = Point2D(broadcast(Base.:*, a, b))

"""
    Point3D{T} <: PointND{3, T}

Create a point in 3D space, with components (x, y, z).
"""
mutable struct Point3D{T} <: PointND{3, T}
    x::T
    y::T
    z::T
end

Point3D() = Point3D(NaN, NaN, NaN)
Point3D(x::AbstractFloat) = Point3D(x, x, x)
Point3D(x::Integer) = Point3D(Float64(x))
Point3D(x::sa.SArray{Tuple{3},T,1,3}) where T = Point3D(x[1], x[2], x[3])

@inline Base.:+(a::Point3D, b::Point3D) = Point3D(broadcast(Base.:+, a, b))
@inline Base.:-(a::Point3D, b::Point3D) = Point3D(broadcast(Base.:-, a, b))
@inline Base.:*(a::Point3D, b::Number) = Point3D(broadcast(Base.:*, b, a))
@inline Base.:*(a::Number, b::Point3D) = Point3D(broadcast(Base.:*, a, b))

"""
    dist(a::AbstractVector, b::AbstractVector)

Calculate Euclidean distance between two vectors.
"""
dist(a::AbstractVector, b::AbstractVector) = la.norm(a-b)

"""
    dist2D(a::AbstractVector, b::AbstractVector)

Calculate Euclidean distance in the XY plane between two points.
"""
dist2D(a::AbstractVector, b::AbstractVector) = la.norm((a-b)[1:2])