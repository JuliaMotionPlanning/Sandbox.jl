#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    AbstractTrajectory

An AbstractTrajectory describes the desired state of a system as function of time, as represented by a [`StateMatrix`](@ref).
"""
abstract type AbstractTrajectory{T <: AbstractFloat} end

"""
    get_start_time(traj::AbstractTrajectory)

Get the first time for which the trajectory is valid.
"""
get_start_time(traj::AbstractTrajectory) = traj.t0

"""
    get_end_time(traj::AbstractTrajectory)

Get the last time for which the trajectory is valid.
"""
get_end_time(traj::AbstractTrajectory) = traj.tf

"""
    get_duration(traj::AbstractTrajectory)

Get the duration of the trajectory.
"""
get_duration(traj::AbstractTrajectory) = get_end_time(traj) - get_start_time(traj)

"""
    set_start_time!(traj::AbstractTrajectory, new_time::Real)

Set the trajectory's valid starting time to `new_time`.
"""
set_start_time!(traj::AbstractTrajectory, new_time::Real) = error("Must implement for your trajectory type.")

"""
    slice(traj::AbstractTrajectory, t0::Real, tf::Real)

Slicing a trajectory from `[t0, tf]` returns a new trajectory that is only valid on the new range.
"""
slice(traj::AbstractTrajectory, t0::Real, tf::Real) = error("Must implement for your trajectory type.")

"""
    evaluate(traj::AbstractTrajectory, time::Real)

Evaluate the trajectory at the desired `time`.
"""
evaluate(traj::AbstractTrajectory, time::Real) = error("Must implement for your trajectory type.")

"""
    get_first(traj::AbstractTrajectory)

Evaluate the trajectory at its first valid time.
"""
get_first(traj::AbstractTrajectory) = evaluate(traj, get_start_time(traj))

"""
    get_last(traj::AbstractTrajectory)

Evaluate the trajectory at its last valid time.
"""
get_last(traj::AbstractTrajectory) = evaluate(traj, get_end_time(traj))

"""
    num_dimensions(traj::AbstractTrajectory)

Return the number of dimensions of the trajectory.
"""
num_dimensions(traj::AbstractTrajectory) = error("Not implemented for an AbstractTrajectory -- you must implement this for your own type.")

"""
    num_derivatives(traj::AbstractTrajectory)

Return the number of derivatives of the trajectory.
"""
num_derivatives(traj::AbstractTrajectory) = error("Not implemented for an AbstractTrajectory -- you must implement this for your own type.")

"""
    discretise(traj::AbstractTrajectory)

Discretise the input trajectory and return a tuple consisting of `(time, Vector{[`StateMatrix`](@ref)})`.
"""
function discretise(traj::AbstractTrajectory, dt::AbstractFloat = 0.01)
    time_points = get_start_time(traj) : dt : get_end_time(traj)
    N = length(time_points)
    setpoints = Vector{StateMatrix}(undef, N)
    for i = 1 : N
        setpoints[i] = evaluate(traj, time_points[i])
    end
    return time_points, setpoints
end

"""
    to3DMatrix(setpoints::Vector{StateMatrix})

Given a vector of setpoints, convert to a 3D matrix to easily slice by time, dimension, or derivative.
"""
function to3DMatrix(setpoints::Vector{StateMatrix})
    num_dim = size(setpoints[1].state, 1)
    num_deriv = size(setpoints[1].state, 2)
    all_states = Array{Float64}(undef, length(setpoints), num_dim, num_deriv)
    for (index, sp) ∈ enumerate(setpoints)
        all_states[index, :, :] = sp.state
    end
    return all_states
end
