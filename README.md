# MotionPlanning.jl
[![pipeline status](https://gitlab.com/kylecarbon/MotionPlanning.jl/badges/master/pipeline.svg)](https://gitlab.com/kylecarbon/MotionPlanning.jl/pipelines)
[![coverage report](https://gitlab.com/kylecarbon/MotionPlanning.jl/badges/master/coverage.svg)](https://codecov.io/gl/kylecarbon/MotionPlanning.jl/branch/master)
[![documentation](https://img.shields.io/badge/docs-latest-blue.svg)](https://kylecarbon.gitlab.io/MotionPlanning.jl/)

This repository contains algorithms and tools for prototyping motion planning for quadrotors. It includes but is not limited to:
* datatypes (setpoint, trajectory, paths, etc.)
* quadrotor dynamics
* trajectory generation
* exploration strategies
