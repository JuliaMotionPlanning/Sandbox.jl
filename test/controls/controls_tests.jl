@testset "Controller" begin
    @testset "Interface" begin
        fc = mp.FakeRigidBodyController()

        @test mp.compute_force(fc) == @SVector [0.0, 0.0, 0.0]
        @test mp.compute_moment(fc) == @SVector [0.0, 0.0, 0.0]
        f, m = mp.compute_wrench(fc)
        @test f == @SVector [0.0, 0.0, 0.0]
        @test m == @SVector [0.0, 0.0, 0.0]

        controllers = Vector{mp.AbstractRigidBodyController}()
        push!(controllers, fc)
        @test mp.compute_force(controllers[1]) == @SVector [0, 0, 0.0]
        @test mp.compute_moment(controllers[1]) == @SVector [0, 0, 0.0]
        f, m = mp.compute_wrench(controllers[1])
        @test f == @SVector [0.0, 0.0, 0.0]
        @test m == @SVector [0.0, 0.0, 0.0]
    end
end