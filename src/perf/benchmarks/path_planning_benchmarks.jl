function gridmap_search_benchmark(map = "free_map_small"; samples = 10000)
    suite = BenchmarkGroup()
    split_map = split(map, ".")
    map_no_ext = split_map[1]
    suite[map_no_ext] = BenchmarkGroup(["bfs", "dijkstra", "astar"])

    functions = [bfs, dijkstra, astar]
    gm = image2gridmap(map, Point2D(0,0), 0.05, smart_load = true)
    s = Point2D(0, 0)
    d = Point2D(0, getheightmetric(gm))
    for func ∈ functions
        # get rid of the prepended module
        split_func_names = split(string(func), ".")
        func_name = split_func_names[end]
        suite[map_no_ext][func_name] = BenchmarkTools.@benchmarkable $func($gm, $s, $d)
    end

    results = BenchmarkTools.run(suite, samples = samples)
end