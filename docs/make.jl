using Documenter, MotionPlanning

makedocs(format = Documenter.HTML(),
         sitename = "MotionPlanning",
         pages = ["Home" => "index.md",
                  "Tutorials" => ["tutorials/poly_opt.md",
                                  "tutorials/path_planner.md"],
									"API" => ["pages/datatypes.md",
                            "pages/path_planning.md",
                            "pages/polynomial_optimisation.md",
                            "pages/utilities.md"],
                  ],
         repo = "https://gitlab.com/kylecarbon/MotionPlanning.jl/blob/{commit}{path}#{line}"
         )
