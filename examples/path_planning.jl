using MotionPlanning
const mp = MotionPlanning
using FileIO

# # Path Planning

# ## Setup: load the map and set start + destination
img_path = joinpath(mp.mpdir(), "test/maps/free_map_small.png")
gm = mp.image2gridmap(img_path, mp.Point2D(0,0), 0.05)
y = mp.getheightmetric(gm)
s = mp.Point2D(0, 0)
d = mp.Point2D(0, y)

# ## BFS
bfs_cell_path, bfs_prev = mp.bfs(gm, s, d)
bfs_path = mp.indices2position(gm, bfs_cell_path)
bfs_img = mp.to_image(gm, bfs_path)
# save("simple_map_bfs.png", bfs_img)

# ## Dijkstra
dijkstra_cell_path, dijkstra_prev = mp.dijkstra(gm, s, d)
dijkstra_path = mp.indices2position(gm, dijkstra_cell_path)
dijkstra_img = mp.to_image(gm, dijkstra_path)
# save(joinpath(home, "Downloads/simple_map_dijkstra.png"), dijkstra_img)

# ## A*
astar_cell_path, astar_prev = mp.astar(gm, s, d)
astar_path = mp.indices2position(gm, astar_cell_path)
astar_img = mp.to_image(gm, astar_path, astar_prev)
# save(joinpath(ENV["HOME"], "Downloads/simple_map_astar.png"), astar_img)

# ## Compare results
# First, let's compare the number of cells:
println("BFS length: $(length(bfs_cell_path.indices))")
println("Dijkstra length: $(length(dijkstra_cell_path.indices))")
println("A* length: $(length(astar_cell_path.indices))")

# Next, let's compare the metric distance of all paths:
println("BFS length: $(mp.arclength(bfs_path))")
println("Dijkstra length: $(mp.arclength(dijkstra_path))")
println("A* length: $(mp.arclength(astar_path))")

# Finally, let's check the number of nodes that were visited.
println("BFS visited: $(length(bfs_prev))")
println("Dijkstra visited: $(length(dijkstra_prev))")
println("A* visited: $(length(astar_prev))")