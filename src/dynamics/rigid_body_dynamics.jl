#= 
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License. =#

mutable struct RigidBodyFixedCoM
    mass::Float64 # kg
    J::SMatrix{3,3,Float64} # moment of inertia matrix, kg m^2
    F::SVector{3,Float64} # Must be the net moment applied to the rigid body at its center of mass in the inertial frame
    M::SVector{3,Float64} # Must be the net force applied to the rigid body at its center of mass in the inertial frame
    function RigidBodyFixedCoM(mass, J, F, M)
        @assert mass > 0
        new(mass, J, F, M)
    end
end

RigidBodyFixedCoM() = RigidBodyFixedCoM(1, Matrix{Float64}(undef, 3, 3), Vector{Float64}(undef, 3), Vector{Float64}(undef, 3))
RigidBodyFixedCoM(mass, J) = RigidBodyFixedCoM(mass, J, Vector{Float64}(undef, 3), Vector{Float64}(undef, 3))

getPositionInd(::Type{RigidBodyFixedCoM}) = 1:3
getPositionInd(a::RigidBodyFixedCoM) = getPositionInd(typeof(a))
getVelocityInd(::Type{RigidBodyFixedCoM}) = 4:6
getVelocityInd(a::RigidBodyFixedCoM) = getVelocityInd(typeof(a))
getQuaternionInd(::Type{RigidBodyFixedCoM}) = 7:10
getQuaternionInd(a::RigidBodyFixedCoM) = getQuaternionInd(typeof(a))
getAngularVelocityInd(::Type{RigidBodyFixedCoM}) = 11:13
getAngularVelocityInd(a::RigidBodyFixedCoM) = getAngularVelocityInd(typeof(a))
sizeStateVector(::Type{RigidBodyFixedCoM}) = 13
sizeStateVector(a::RigidBodyFixedCoM) = sizeStateVector(typeof(a))

function simulate(du, u, p::RigidBodyFixedCoM, t)
    # Get states from state vector
    # position = view(x, getPositionInd(p))
    velocity = view(u, getVelocityInd(p))
    # Re-normalise the quaternion after its been integrated to account for numerical drift
    q = Quaternion(u[getQuaternionInd(p)]..., true)
    ω = u[getAngularVelocityInd(p)]

    # rotational dynamics
    q_dot = 0.5 * q * Quaternion(ω)
    ω_dot = p.J \ (p.M - ω × (p.J * ω))
    # linear dynamics
    v_dot = p.F / p.mass

    du[getPositionInd(p)] = velocity
    du[getVelocityInd(p)] = v_dot
    du[getQuaternionInd(p)] = [q_dot.s, q_dot.v1, q_dot.v2, q_dot.v3]
    du[getAngularVelocityInd(p)] = ω_dot
end

function simulateRot(du, u, p::RigidBodyFixedCoM, t)
    # position = u[getPositionInd(p)]
    velocity = u[4:6]
    ω = u[7:9]
    R = reshape(u[10:18], (3, 3))

    # rotational dynamics
    Rdot = R * vectorToSkew(ω)
    ω_dot = p.J \ (p.M - ω × (p.J * ω))
    # linear dynamics
    v_dot = p.F / p.mass

    du[1:3] = velocity
    du[4:6] = v_dot
    du[7:9] = ω_dot
    du[10:18] = Rdot[:]
end
