#=
Copyright [2020] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    AbstractMotorModel

WIP: still figuring out which interface works best for the motor model.

An AbstractMotorModel must compute:
1. angular velocity of all rotors as a function of time, ``\\omega_i(t)`` for the i-th motor
1. body forces and moments as a function of a rotor's angular velocity

``(F^B, M^{B/B_{cm}}) = \\sum_i f(omega_i)``
"""
abstract type AbstractMotorModel end

"""
    computeBodyWrench

Compute the wrench (`F`, `M`) in the body-frame.
"""
compute_body_wrench(motor::AbstractMotorModel) = computeBodyWrench(motor)

"""
    sizeStateVector

Return the size of the state vector of the motor model.
"""
sizeStateVector(motor::AbstractMotorModel) = sizeStateVector(motor)

function simulate(du, u, p::AbstractMotorModel, t)
    simulate(du, u, p, t)
end

mutable struct FakeMotorModel <: AbstractMotorModel
    F::MVector{3, Float64} # Must be the net moment applied to the rigid body at its center of mass in the body frame
    M::MVector{3, Float64} # Must be the net force applied to the rigid body at its center of mass in the body frame
end

FakeMotorModel() = FakeMotorModel(zeros(SVector{3}), zeros(SVector{3}))

compute_body_wrench(motor::FakeMotorModel) = motor.F, motor.M
sizeStateVector(motor::FakeMotorModel) = 1
function simulate(du, u, p::FakeMotorModel, t)
    du[1] = 0
end

"""
    XMotorModelParams(τ, k_f, k_m, quad_arm)

Parameters for a motor configuration 4 motors configured in a perfect "X", where each
motor is equi-distant from the center of mass and they are spaced at 45 degree increments from the body x-axis.

Parameters include:
* τ: [seconds] time constant as represented by a first-order system
* k_f: F = k_f ω^2
* k_m: M = k_m ω^2
* quad_arm: [meters] length of a quadrotor arm

Assuming XYZ axes that are forward-right-down (FRD), the motors are numbered as:
1. Forward-right, spinning in the -Z direction (or ccw).
1. Back-left, spinning in the -Z direction (or ccw).
1. Forward-left, spinning in the Z direction (or cw).
1. Back-right, spinning in the Z direction (or cw).

Note: all units in SI.
"""
mutable struct XMotorModelParams
    τ::Float64 # [seconds] time constant for first-order system
    k_f::Float64 # F = k_f ω^2
    k_m::Float64 # M = k_m ω^2
    quad_arm::Float64 # length of the quadrotor arm
    rot2forces::Matrix{Float64} # [F M]^T = rpm2forces * ω
    forces2rot::Matrix{Float64}
end

function XMotorModelParams(τ, k_f, k_m, quad_arm)
    le = quad_arm * √2 / 2 # effective moment arm
    a = le * k_f
    rot2forces = [
        0 0 0 0
        0 0 0 0
        -k_f -k_f -k_f -k_f
        -a a a -a
        a -a a -a
        k_m k_m -k_m -k_m
    ]
    forces2rot = pinv(rot2forces)
    return XMotorModelParams(τ, k_f, k_m, quad_arm, rot2forces, forces2rot)
end

"""
    XMotorModel

Member variables include:
* `params`: see [`XMotorModelParams`](@ref)
* `ωr`: 4-element vector of reference angular rate in [rad/s]
"""
mutable struct XMotorModel <: AbstractMotorModel
    params::XMotorModelParams
    ω::MVector{4, Float64} # motor angular velocity in [rad/s]
    ωr::MVector{4, Float64} # reference angular velocity in [rad/s]
end

function XMotorModel(params::XMotorModelParams, ωr)
    XMotorModel(params, zeros(MVector{4}), ωr)
end

function XMotorModel(params::XMotorModelParams)
    XMotorModel(params, zeros(MVector{4}), zeros(MVector{4}))
end

function compute_body_wrench(motor::XMotorModel)
    a = motor.params.rot2forces * motor.ω.^2
    F = a[1:3]
    M = a[4:6]
    return F, M
end

function simulate(du, u, p::XMotorModel, t)
    p.ω = u
    du .= (p.ωr .- u) / p.params.τ
end

sizeStateVector(motor::XMotorModel) = length(motor.ω)
