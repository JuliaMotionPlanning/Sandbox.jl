#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

module MotionPlanning

using Reexport

@reexport using StaticArrays
const sa = StaticArrays
@reexport using Rotations
@reexport using CoordinateTransformations
# used for Quaternion math over Rotations, given that operator overloads in Rotations
# will normalise quaternions by default
using Quaternions

@reexport using LinearAlgebra
const la = LinearAlgebra

import Polynomials
const po = Polynomials

import Random
const ra = Random

import DataStructures
const ds = DataStructures

import Atmosphere
const am = Atmosphere

@reexport using PlotlyJS
@reexport using Nord

export Dimensions, Derivatives

export StateMatrix
export AbstractTrajectory, PolynomialTrajectory, PiecewiseTrajectory
export get_start_time, get_end_time, get_duration, set_start_time!, getN
export slice, evaluate, get_first, get_last, num_dimensions, num_derivatives
export discretise

export PointND, Point2D, Point3D
export dist, dist2D

export GridmapND, Gridmap2D, Gridmap3D
export get_origin, get_resolution
export getwidthcells, getheightcells, getdepthcells
export getwidthmetric, getheightmetric, getdepthmeters
export mapping

export AbstractPath2D, Path2D, CellPath2D
export arclength

"""
    greet()

Welcome the user to MotionPlanning.jl.
"""
greet() = print("Welcome to MotionPlanning.jl.\n")

mpdir() = abspath(joinpath(@__DIR__, ".."))

```@meta
DocTestSetup = quote
    using MotionPlanning
    const mp = MotionPlanning
end
```

include("datatypes/datatypes.jl")
include("planning/planning.jl")
include("utilities/utilities.jl")
include("dynamics/dynamics.jl")
include("controls/controls.jl")
include("perf/perf.jl")

end # module
