@testset "Point" begin
    @testset "Construction" begin
        @test all(isnan, mp.Point3D())
        @test iszero(mp.Point3D(0))
    end
    @testset "Operators" begin
        a = mp.Point2D(2)
        b = mp.Point2D(4)
        c = a .* b
        @test c[1] == c[2] == 8

        d = b - a
        @test d[1] == d[2] == 2
        d = b + a
        @test d[1] == d[2] == 6
        e = 2 * a
        @test e[1] == e[2] == 4
        e = a * 2
        @test e[1] == e[2] == 4

        a = mp.Point3D(5)
        b = 2 * a
        @test b[1] == b[2] == b[3] == 10
        b = a * 2
        @test b[1] == b[2] == b[3] == 10
        b = a + a
        @test b[1] == b[2] == b[3] == 10
        b = a - a
        @test b[1] == b[2] == b[3] == 0
    end
    @testset "Distance" begin
        @test iszero(mp.dist(mp.Point3D(0), mp.Point3D(0)))
        @test iszero(mp.dist2D(mp.Point3D(0), mp.Point3D(0)))
        @test mp.dist(mp.Point3D(0), mp.Point3D(1)) == sqrt(3)
        @test mp.dist2D(mp.Point3D(0), mp.Point3D(1)) == sqrt(2)
    end
end