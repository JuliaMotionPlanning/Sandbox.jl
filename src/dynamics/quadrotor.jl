#=
Copyright [2019] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#


mutable struct SimpleQuadrotor
    plant::RigidBodyFixedCoM
    motor::AbstractMotorModel
end

getPositionInd(::Type{SimpleQuadrotor}) = getPositionInd(RigidBodyFixedCoM())
getPositionInd(a::SimpleQuadrotor) = getPositionInd(a.plant)
getVelocityInd(::Type{SimpleQuadrotor}) = getVelocityInd(RigidBodyFixedCoM())
getVelocityInd(a::SimpleQuadrotor) = getVelocityInd(a.plant)
getQuaternionInd(::Type{SimpleQuadrotor}) = getQuaternionInd(RigidBodyFixedCoM())
getQuaternionInd(a::SimpleQuadrotor) = getQuaternionInd(a.plant)
getAngularVelocityInd(::Type{SimpleQuadrotor}) = getAngularVelocityInd(RigidBodyFixedCoM())
getAngularVelocityInd(a::SimpleQuadrotor) = getAngularVelocityInd(a.plant)

function getMotorInd(a::SimpleQuadrotor)
    first = getAngularVelocityInd(a)[end] + 1
    last = first + sizeStateVector(a.motor) - 1
    @assert last >= first "Last index must be greater than first index."
    return first:last
end
sizeStateVector(a::SimpleQuadrotor) = getMotorInd(a)[end]

function simulate(du, u, p::SimpleQuadrotor, t)
    # quadrotor parameters
    position = u[getPositionInd(p)]
    velocity = u[getVelocityInd(p)]
    ω_q = u[getAngularVelocityInd(p)]
    q = Quat(u[getQuaternionInd(p)]...)
    # motor parameters
    ω_m = u[getMotorInd(p)]

    # compute desired force for position control
    # pos_ctrl = computePositionControl(p, p.controller, p.trajectory, u, t)
    # TODO: compute desired moment for attitude control
    # TODO: compute motor RPMs from desired moment

    # get forces and moments
    F_b, M_b = compute_body_wrench(p.motor)
    # convert body force to inertial
    # rot = inv(LinearMap(R))
    p.plant.F = inv(q)*F_b + SVector(0.0, 0.0, p.plant.mass*am.g)
    p.plant.M = M_b

    du_rigid_body = zeros(sizeStateVector(p.plant))
    u_plant = u[getPositionInd(p)[1] : getAngularVelocityInd(p)[end]]
    simulate(du_rigid_body, u_plant, p.plant, t)
    du[1:sizeStateVector(p.plant)] = du_rigid_body

    du_motor = zeros(sizeStateVector(p.motor))
    motor_ind = getMotorInd(p)
    u_motor = u[motor_ind]
    simulate(du_motor, u_motor, p.motor, t)
    du[motor_ind] = du_motor
end
