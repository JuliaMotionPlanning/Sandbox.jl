#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    StateMatrix{Dim, Derivative, T <: AbstractFloat}
    StateMatrix{Dim, Derivative}(undef)
    StateMatrix{Dim, Derivative}(x::Real)

If a system has a state vector \$x \\in \\text{R}^{\\text{Dim}}\$, where its derivative is defined as \$x' = \\frac{dx}{dt}\$,
then the StateMatrix is defined as \$[x, x', x'', ..., x^\\text{Derivative - 1}]\$, i.e. \$ \\text{StateMatrix} \\in \\text{R}^{\\text{Dim} \\times \\text{Derivative}} \$.

# Examples
```jldoctest; output = false
statematrix = StateMatrix(MMatrix{3,2,Float16}(undef)) # a setpoint has 3 dimensions (XYZ), contains position/velocity/acceleration (the [0, 2] derivatives), uses Float16
statematrix = StateMatrix{3, 2}(undef) # same as above but defaults to Float64
statematrix = StateMatrix{3, 3}(5) # creates a 3 x 3 StateMatrix that is initialised to 5 for each element, defaults to Float64

# output
StateMatrix{3,3,Float64}([5.0 5.0 5.0; 5.0 5.0 5.0; 5.0 5.0 5.0])
```
"""
mutable struct StateMatrix{Dim, Derivative, T <: AbstractFloat}
    state::sa.MMatrix{Dim, Derivative, T}
end

StateMatrix{Dim, Derivative}(undef) where {Dim, Derivative} = StateMatrix(sa.MMatrix{Dim, Derivative, Float64}(undef))
StateMatrix{Dim, Derivative}(x::Real) where {Dim, Derivative} = StateMatrix{Dim, Derivative, Float64}(Float64(x) * ones(sa.MMatrix{Dim, Derivative, Float64}))
"""
    zeros(::Type{StateMatrix{Dim, Derivative}})
    zeros(::Type{StateMatrix{Dim, Derivative, T}})

Convenience functions to create a StateMatrix of all zeros.
If `T` is not specified, default to `Float64`.

# Examples
```jldoctest
julia> a = zeros(MotionPlanning.StateMatrix{3, 2});
```
"""
__zeros() = @error "Dummy function used to generate documentation."
Base.zeros(::Type{StateMatrix{Dim, Derivative}}) where {Dim, Derivative} = StateMatrix{Dim, Derivative, Float64}(zeros(sa.MMatrix{Dim, Derivative, Float64}))
Base.zeros(::Type{StateMatrix{Dim, Derivative, T}}) where {Dim, Derivative, T} = StateMatrix{Dim, Derivative, T}(zeros(sa.MMatrix{Dim, Derivative, T}))

"""
    size(mat::StateMatrix)

Return a tuple of the `(rows, cols)` of a `StateMatrix`.
# Examples
```
julia> a = zeros(MotionPlanning.StateMatrix{3, 2});

julia> rows, cols = MotionPlanning.size(a)
(3, 2)
```
"""
__size() = @error "Dummy function used to generate documentation."
Base.size(mat::StateMatrix) = size(mat.state)
dimensions(mat::StateMatrix) = size(mat.state, 1)
derivatives(mat::StateMatrix) = size(mat.state, 2)

"""
    getindex(a::StateMatrix, ...)
    setindex!(a::StateMatrix, ...)

StateMatrix implements the interfaces required (i.e. `getindex` & `setindex!`) such that one can index StateMatrices.

# Examples
```jldoctest
julia> a = zeros(MotionPlanning.StateMatrix{3, 2});

julia> a[1, 2] = 42
42

julia> a[1, :]
2-element MArray{Tuple{2},Float64,1,2}:
  0.0
 42.0
```
"""
__getindex() = @error "Dummy function used to generate documentation."
Base.getindex(mat::StateMatrix, i::Integer) = getindex(mat.state, i)
Base.getindex(mat::StateMatrix, I::Vararg{Int, 2}) = getindex(mat.state, I[1], I[2])
Base.setindex!(mat::StateMatrix, v, i::Int) = setindex!(mat.state, v, i)
Base.setindex!(mat::StateMatrix, v, I::Vararg{Int, 2}) = setindex!(mat.state, v, I[1], I[2])

position(mat::StateMatrix) = mat.state[:, Derivatives.POSITION + 1]
velocity(mat::StateMatrix) = mat.state[:, Derivatives.VELOCITY + 1]
acceleration(mat::StateMatrix) = mat.state[:, Derivatives.ACCELERATION + 1]
jerk(mat::StateMatrix) = mat.state[:, Derivatives.JERK + 1]
snap(mat::StateMatrix) = mat.state[:, Derivatives.SNAP + 1]
