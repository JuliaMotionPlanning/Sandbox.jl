#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

using ImageMagick, FileIO, Images, ImageView, Colors, ColorTypes, FixedPointNumbers

"""
    image2gridmap(filename::String, origin::Point2D, resolution::AbstractFloat))

Load an image found at `filename` into a [`Gridmap2D`](@ref). When viewing the image,
the origin will be in the bottom-left corner, as if viewing a standard XY plot.
1. `filename`: location of image to load
1. `origin`: coordinates [meters] given to the origin of the gridmap
1. `resolution`: the width & height [meters] of a single cell in the gridmap
"""
function image2gridmap(filename::String, origin::Point2D = Point2D(0, 0), resolution::Float64 = 0.05; smart_load = false)
    if smart_load
        tmp_img = loadmap(filename)
    else
        tmp_img = load(filename)
    end
    tmp_img = convert(Matrix{RGB{N0f8}}, tmp_img)
    # take the transpose, then reverse to get the normal cartesian XY view (X goes to right, Y goes up)
    img = reverse(tmp_img', dims = 2)
    gridmap = Gridmap2D()
    gridmap.resolution = resolution
    gridmap.origin = origin
    gridmap.data = Matrix{UInt8}(undef, size(img))
    for (index, cell) ∈ enumerate(img)
        if cell == colorant"white"
            gridmap.data[index] = Mapping.FREE
        elseif cell == colorant"black"
            gridmap.data[index] = Mapping.LETHAL
        else
            gridmap.data[index] = Mapping.NO_INFORMATION
        end
    end
    return gridmap
end

function gridmap2image!(img::Matrix{RGB{N0f8}}, gridmap_data::Matrix)
    for (index, cell) ∈ enumerate(gridmap_data)
        if cell == Mapping.FREE
            img[index] = colorant"white"
        elseif cell == Mapping.LETHAL
            img[index] = colorant"black"
        elseif cell == Mapping.NO_INFORMATION
            img[index] = colorant"grey"
        else
            @error "Unknown cell value of $cell at index: $index -- not writing to file."
        end
    end
end

function path2image!(img::Matrix{RGB{N0f8}}, path::Path2D, gridmap::Gridmap2D)
    for point ∈ path.points
        ind = get_cartesian_index(gridmap, point)
        img[ind] = colorant"green1"
    end
end

function path2image!(img::Matrix{RGB{N0f8}}, path::CellPath2D, gridmap::Gridmap2D)
    for ind ∈ path.indices
        img[ind] = colorant"green1"
    end
end

function visitedcells2image!(img::Matrix{RGB{N0f8}}, visited::Set{CartesianIndex})
    for cell ∈ visited
        img[cell] = colorant"blue"
    end
end

function to_image(gridmap::Gridmap2D, path::AbstractPath2D)
    return to_image(gridmap, path, Set{CartesianIndex}())
end

function to_image(gridmap::Gridmap2D, path::AbstractPath2D, visited::Set{CartesianIndex})
    gridmap_data = deepcopy(gridmap.data)
    output = Matrix{RGB{N0f8}}(undef, size(gridmap_data))
    gridmap2image!(output, gridmap_data)
    visitedcells2image!(output, visited)
    path2image!(output, path, gridmap)
    # reverse, then take the transpose, to get a standard cartesian XY view back (x to right, y up)
    # see image2gridmap also
    output = reverse(output, dims = 2)
    output = output'
    return output
end

function loadmap(map = "free_map", ext="png")
    img_path = joinpath(mpdir(), "test/maps", map * "." * ext)
    return load(img_path)
end