"""
    vectorToSkew(a::Vector)

Given a vector, return a [skew-symmetric matrix](https://en.wikipedia.org/wiki/Skew-symmetric_matrix#Cross_product)
that represents a cross-product multiplication, i.e. given two vectors ``a`` and ``b``

```math
a \\times b = \\hat{a} \\times b
```
where ``\\hat{a}`` is the skew-symmetric matrix of ``a``.

Input:
1. `a`: vector of length 3

Output:
1. `ahat`: 3x3 skew-symmetric matrix of `a`
"""
function vectorToSkew(x)
    if length(x) != 3
        throw(AssertionError("Input must be a vector of length 3. Instead, input was: $x"))
    end
    xhat = [0 -x[3] x[2]
            x[3] 0 -x[1]
            -x[2] x[1] 0]
    @assert (3, 3) == size(xhat) "Skew operator must return a 3x3 matrix"
    return xhat
end

function isRotationQuaternion(q)
    has_4_elements = length(q) == 4
    has_norm_1 = isapprox(norm(q), 1.0)
    if !has_4_elements
        @info "Length of input is $(length(q))"
    end
    if !has_norm_1
        @info "Norm of input is $(norm(q))"
    end
    return has_4_elements && has_norm_1
end

function isRotationMatrix(R)
    det_eq_1 = isapprox(det(R), 1.0)
    transpose_id = isapprox(R * transpose(R), I)
    if !det_eq_1
        @info "Determinant = $(det(R))"
    end
    if !transpose_id
        @info "R * R' = $(R * transpose(R))"
    end
    return det_eq_1 && transpose_id
end