let traj
    @testset "PiecewiseTrajectory" begin
        @testset "Construction" begin
            a = Vector{mp.PolynomialTrajectory{Float64}}()
            @test_throws AssertionError mp.PiecewiseTrajectory(a)
        end
        @testset "Evaluate" begin
            traj_duration = 1;
            a = mp.PolynomialTrajectory{Float64}(traj_duration, mp.po.Poly([0, 2.0]))
            b = mp.PolynomialTrajectory{Float64}(traj_duration, mp.po.Poly([2, 2.0]))
            c = mp.PolynomialTrajectory{Float64}(traj_duration, mp.po.Poly([4, 2.0]))
            d = [a, b, b]
            traj = mp.PiecewiseTrajectory(d)

            temp = [traj, traj]
            double_traj = mp.PiecewiseTrajectory(temp)

            @test mp.get_start_time(traj) == 0
            @test mp.get_end_time(traj) == length(d) * traj_duration
            @test mp.get_end_time(double_traj) == length(d) * 2 * traj_duration
            @test mp.get_start_time(b) == 0
            @test mp.get_end_time(b) == traj_duration

            d = [a, b, c]
            traj = mp.PiecewiseTrajectory(d)
            mp.set_start_time!(traj, 1)

            sp = mp.evaluate(traj, mp.get_start_time(traj) - 0.5)
            @test mp.position(sp) == [0, 0, 0]

            sp = mp.evaluate(traj, mp.get_start_time(traj))
            @test mp.position(sp) == [0, 0, 0]

            sp = mp.evaluate(traj, mp.get_start_time(traj) + 0.5)
            @test mp.position(sp) == [1, 1, 1]

            sp = mp.evaluate(traj, mp.get_start_time(traj) + 1.1)
            @test mp.position(sp) == [2.2, 2.2, 2.2]

            sp = mp.evaluate(traj, mp.get_start_time(traj) + 2.9)
            @test mp.position(sp) == [5.8, 5.8, 5.8]
        end
        @testset "Utilities" begin
            @test mp.num_dimensions(traj) == 3
            @test mp.num_derivatives(traj) == 2
            @test mp.getN(traj) == 2
        end
        @testset "Derivative" begin
            traj_d = mp.derivative(traj)
            @test mp.num_derivatives(traj_d) == (mp.num_derivatives(traj) - 1)
        end
        @testset "Copying" begin
            new_traj = mp.copy(traj)
            mp.set_start_time!(new_traj, 10)
            @test mp.get_start_time(new_traj) == 10
            @test mp.get_start_time(traj) == 1
        end
    end
end