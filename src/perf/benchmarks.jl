using BenchmarkTools, LaTeXStrings

ra.seed!(1)

include("benchmarks/poly_opt_benchmarks.jl")
include("benchmarks/path_planning_benchmarks.jl")

function plot_benchmark(results; test_name::String, benchmark_field::Symbol, kwargs...)
    scale = 1
    units = ""
    sigfigs = 3
    if benchmark_field == :time || benchmark_field == :gctime
        # benchmarktools reports time in nanoseconds
        scale = 1e-3
        units = "μs"
        sigfigs = 3
    elseif benchmark_field == :memory
        scale = 1 / 1024^2
        units = "MiB"
    elseif benchmark_field == :allocs
        scale = 1
        units = ""
        sigfigs = 3
    else
        throw(ArgumentError("[plot_benchmark] not support for $benchmark_field."))
    end

    x_vals = []
    median_results = Vector{Float64}()
    labels = Vector{String}()
    for (x_val, result) ∈ results[test_name]
        push!(x_vals, x_val)
        median_result = getfield(BenchmarkTools.median(result), benchmark_field)
        scaled_median_result = median_result * scale
        push!(median_results, scaled_median_result)
        push!(labels, string(round(scaled_median_result, sigdigits = 3)) * " " * units)
    end

    trace = PlotlyJS.scatter(;x = x_vals,
                    y = median_results,
                    text = labels,
                    textposition = "top center",
                    mode = "markers+text")
    layout = PlotlyJS.Layout(;showgrid = true, kwargs...)
    return PlotlyJS.plot([trace], layout)
end
