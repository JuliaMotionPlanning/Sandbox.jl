#!/usr/bin/env julia
using Pkg

# Set MotionPlanning as primary environment for package manager
cur_dir = @__DIR__
mp_dir = abspath(joinpath(cur_dir, "../"))
Pkg.activate(mp_dir)
Pkg.instantiate()

# HACK: https://github.com/JuliaImages/ImageView.jl/pull/156
# this is necessary to get ImageMagick to play nicely
using ImageMagick

import MotionPlanning
const mp = MotionPlanning