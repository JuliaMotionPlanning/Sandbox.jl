#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

abstract type AbstractPath end
abstract type AbstractPath2D <: AbstractPath end

"""
    Path2D

A 2D metric path to traverse (e.g. in XY).
"""
mutable struct Path2D <: AbstractPath2D
    points::Vector{Point2D}
end
Path2D() = Path2D(Vector{Point2D}())

"""
    CellPath2D <: AbstractPath2D

A 2D cell/node-based path to traverse (e.g. in XY).
"""
mutable struct CellPath2D <: AbstractPath2D
    indices::Vector{CartesianIndex{2}}
end
CellPath2D() = CellPath2D(Vector{CartesianIndex{2}}())

mutable struct Path3D <: AbstractPath
    points::Vector{Point3D}
end

"""
    arclength(path::AbstractPath)

Return the sum of the Euclidean distance between adjacent points in a path.
"""
function arclength(path::AbstractPath)
    num_points = length(path.points)
    if num_points == 1
        return 0
    end
    sum = 0
    for i = 2:num_points
        sum += dist(path.points[i], path.points[i-1])
    end
    return sum
end