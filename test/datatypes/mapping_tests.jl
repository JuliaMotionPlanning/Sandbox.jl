@testset "Gridmap" begin
    @testset "Gridmap2D Construction" begin
        a = mp.Gridmap2D()
        map_origin = mp.get_origin(a)
        @test all(isnan, map_origin)
        @test isnan(mp.get_resolution(a))
        @test mp.size(a) == (0, 0)
        @test mp.getwidthcells(a) == 0
        @test mp.getheightcells(a) == 0
    end
    @testset "Gridmap3D Construction" begin
        map = mp.Gridmap3D()
        map_origin = mp.get_origin(map)

        @test all(isnan, map_origin)
        @test isnan(mp.get_resolution(map))
        @test mp.size(map) == (0, 0, 0)
        @test mp.getwidthcells(map) == 0
        @test mp.getheightcells(map) == 0
        @test mp.getdepthcells(map) == 0
        @test isnan(mp.getdepthmeters(map))
    end
end