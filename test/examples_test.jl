# simply run these examples to make sure they run
ex_poly_opt = joinpath(mp.mpdir(), "examples/polynomial_optimisation.jl")
include(ex_poly_opt)

ex_path_planning = joinpath(mp.mpdir(), "examples/path_planning.jl")
include(ex_path_planning)

generate_literate = joinpath(mp.mpdir(), "examples/generate_literate.jl")
include(generate_literate)