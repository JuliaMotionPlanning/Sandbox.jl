# Datatypes
!!! note
    If a function definition has prepended underscores, e.g. `__zeros`, ignore them.
    They are added for functions which I imported and overloaded from the `Base` library,
    and I haven't figured out how to generate documentation for my overloads only. As such,
    I'm resorting to this dumb hack for now.

```@index
Pages   = ["datatypes.md"]
```
```@meta
DocTestSetup = quote
    using MotionPlanning
    using StaticArrays
end
```
## Basic Enums/Modules
```@docs
MotionPlanning.Dimensions
MotionPlanning.Derivatives
MotionPlanning.Mapping
```
## Points and Paths
```@docs
MotionPlanning.PointND
MotionPlanning.Point2D
MotionPlanning.Point3D
MotionPlanning.Path2D
MotionPlanning.CellPath2D
MotionPlanning.dist
MotionPlanning.dist2D
```
## StateMatrix and Trajectory
```@docs
MotionPlanning.StateMatrix
MotionPlanning.__zeros
MotionPlanning.__size
MotionPlanning.__getindex
MotionPlanning.AbstractTrajectory
MotionPlanning.PolynomialTrajectory
MotionPlanning.PiecewiseTrajectory
MotionPlanning.get_start_time
MotionPlanning.get_end_time
MotionPlanning.get_duration
MotionPlanning.set_start_time!
MotionPlanning.slice
MotionPlanning.evaluate
MotionPlanning.get_first
MotionPlanning.get_last
MotionPlanning.discretise
```
## Gridmaps
```@docs
MotionPlanning.GridmapND
MotionPlanning.Gridmap2D
MotionPlanning.get_origin
MotionPlanning.get_resolution
MotionPlanning.size
MotionPlanning.getwidthcells
MotionPlanning.getheightcells
MotionPlanning.getwidthmetric
MotionPlanning.getheightmetric
```
