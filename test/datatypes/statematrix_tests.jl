@testset "StateMatrix" begin
    @testset "Construction" begin
        sm0 = mp.StateMatrix{3, 5}(undef)
        @test mp.size(sm0) == (3, 5)

        sm1 = mp.StateMatrix{3, 5}(3)
        @test mp.size(sm1) == (3, 5)
        @test sm1[1,1] == 3

        sm2 = mp.zeros(mp.StateMatrix{5,5})
        @test sm2[1,1] == 0
    end
    @testset "Getting/setting indices" begin
        sm = mp.StateMatrix{3,3}(undef)
        sm[3] = 1
        @test sm[3] == 1
        @test sm[3,1] == 1
    end
    @testset "Getters" begin
        a = sa.MMatrix{4, 5}([1 2 3.0 4.0 5.0;
             1 2 3 4 5;
             1 2 3 4 5;
             1 2 3 4 5])
        sm = mp.StateMatrix(a)
        pos = mp.position(sm)
        vel = mp.velocity(sm)
        acc = mp.acceleration(sm)
        jerk = mp.jerk(sm)
        snap = mp.snap(sm)
        @test pos == [1, 1, 1.0, 1]
        @test vel == [2, 2, 2.0, 2]
        @test acc == [3, 3, 3.0, 3]
        @test jerk == [4.0, 4, 4, 4]
        @test snap == [5.0, 5, 5, 5]
    end
    @testset "Helper functions for size" begin
        dim = 3
        deriv = 5
        sm0 = mp.zeros(mp.StateMatrix{dim,deriv,Float64})
        @test mp.dimensions(sm0) == dim
        @test mp.derivatives(sm0) == deriv
    end
end
