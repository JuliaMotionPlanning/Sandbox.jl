@testset "Helper methods for Polynomials" begin
    poly = po.Poly([1, 2, 3])
    @test po.Poly([2, 6]) == mp.polyder(poly, 1)
    @test po.Poly([6]) == mp.polyder(poly, 2)

    @test_throws DomainError mp.polyder(poly, -1)
end

let poly
@testset "PolynomialTrajectory" begin
    @testset "Construction" begin
        @test_throws Exception mp.PolynomialTrajectory(1, 0, mp.po.Poly([1, 2.0]))

        poly = mp.PolynomialTrajectory(10.0, mp.Point3D(1))
        mp.PolynomialTrajectory()
        mp.PolynomialTrajectory(5, [1, 2.0])
    end
    @testset "Set start time" begin
        new_traj = mp.set_start_time!(poly, 2)
        @test new_traj === poly
        mp.set_start_time!(poly, 0)
    end

    @testset "Get time & duration" begin
        @test mp.get_start_time(poly) == 0
        @test mp.get_end_time(poly) == 10
        @test mp.get_duration(poly) == 10
    end

    @testset "Get setpoint" begin
    	b = mp.evaluate(poly, 0.0)
    	@test mp.position(b) == [1,1,1]
    	b = mp.evaluate(poly, 5)
    	@test mp.position(b) == mp.Point3D(1)
    end

    @testset "Slice" begin
        @test_throws Exception mp.slice(poly, 1, 0)
        @test_throws Exception mp.slice(poly, 11, 12)
        new_traj = mp.slice(poly, 5, 6)
        @test mp.get_start_time(new_traj) == 5
        @test new_traj != poly
    end

    @testset "Inspect" begin
        @test mp.num_dimensions(poly) == 3
        @test mp.num_derivatives(poly) == 1
        @test mp.getN(poly) == 1
    end

    @testset "Derivative" begin
        traj = mp.PolynomialTrajectory{Float64}(0.0, 1.0, mp.po.Poly([1,2,3,4.0]))
        traj_d = mp.derivative(traj)
        @test mp.num_derivatives(traj_d) == (mp.num_derivatives(traj) - 1)
        @test mp.num_dimensions(traj_d) == mp.num_dimensions(traj)

        @test_throws DomainError mp.derivative(traj, -1)
    end
end
end