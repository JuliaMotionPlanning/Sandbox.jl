@testset "RigidBodyDynamics" begin
    @testset "Utilities" begin
        mp.RigidBodyFixedCoM()

        mass = 1.0
        Jxx = 2.0; Jyy = 2.0; Jzz = 2.0;
        J = sa.SA_F64[Jxx 0 0;
               0 Jyy 0;
               0 0 Jzz]
        body = mp.RigidBodyFixedCoM(mass, J)

        @test mp.sizeStateVector(body) == 13
        @test mp.sizeStateVector(mp.RigidBodyFixedCoM) == 13
    end
    @testset "System with fixed CoM, diagonal J, constant force & moment" begin
        using DifferentialEquations, Quaternions, Rotations

        mass = 2.0
        Jxx = 2.0; Jyy = 2.0; Jzz = 2.0;
        J = sa.SA_F64[Jxx 0 0;
               0 Jyy 0;
               0 0 Jzz]
        Fx = 1; Fy = 2; Fz = 3;
        F = sa.SA_F64[Fx, Fy, Fz]

        Mx = 0.25; My = 0.15; Mz = -0.05;
        M1 = sa.SA_F64[Mx, 0, 0]
        M2 = sa.SA_F64[0, My, 0]
        M3 = sa.SA_F64[0, 0, Mz]
        Moptions = [M1, M2, M3]

        xₒ = [2, -1, 0.5]
        vₒ = [-.1, 0, 0.0]
        qₒ = [1.0, 0, 0, 0.0]
        w1 = [-0.2, 0.0, 0.0]
        w2 = [0.0, 0.1, 0.0]
        w3 = [0.0, 0.0, 0.05]
        Ω = [w1, w2, w3]

        tf = 5.0
        tspan = (0.0, tf)
        for (M, ωₒ) ∈ zip(Moptions, Ω)
            rb = mp.RigidBodyFixedCoM(mass, J, F, M)
            uₒ = [xₒ; vₒ; qₒ; ωₒ]

            prob_quat = ODEProblem(mp.simulate, uₒ, tspan, rb)
            sol_quat = solve(prob_quat, Tsit5(), dtmax = 0.1)

            accel = [Fx, Fy, Fz] / mass
            pos(t) = 0.5 * accel * t^2 + vₒ * t + xₒ
            vel(t) = accel * t + vₒ
            @assert isdiag(J) "J must be diagonal for kinematics equations to be correct."
            α = J \ M
            rot(t) = 0.5 * α * t^2 + ωₒ * t
            ω(t) = α * t + ωₒ

            ϵ = 1e-8
            for (t, u) ∈ zip(sol_quat.t, sol_quat.u)
                sim_pos = u[mp.getPositionInd(rb)]
                expected_pos = pos(t)
                @test isapprox(norm(expected_pos - sim_pos), 0.0, atol = ϵ)

                sim_vel = u[mp.getVelocityInd(rb)]
                expected_vel = vel(t)
                @test isapprox(norm(expected_vel - sim_vel), 0.0, atol = ϵ)

                qf_tmp = u[mp.getQuaternionInd(rb)]
                @test mp.isRotationQuaternion(qf_tmp)
                qf = Quat(qf_tmp...)
                r_xyz_quat = RotXYZ(qf)
                @test mp.isRotationMatrix(r_xyz_quat)
                rpy = [r_xyz_quat.theta1, r_xyz_quat.theta2, r_xyz_quat.theta3]
                @test isapprox(norm(rpy - rot(t)), 0.0, atol = ϵ)

                ωsim = u[mp.getAngularVelocityInd(rb)]
                @test isapprox(norm(ωsim - ω(t)), 0.0, atol = ϵ)
            end
        end
    end

    @testset "Compare dynamics of quaternion vs. rotation matrix" begin
        using DifferentialEquations, Quaternions, Rotations

        mass = 2.0
        Jxx = 2.0; Jyy = 3.0; Jzz = 4.0;
        Jxy = 0.1; Jxz = -0.1; Jyz = 0.25;
        J = sa.SA_F64[Jxx Jxy Jxz;
               Jxy Jyy Jyz;
               Jxz Jyz Jzz]
        Fx = 1; Fy = 2; Fz = 3;
        F = sa.SA_F64[Fx, Fy, Fz]

        Mx = .25; My = 1; Mz = -1;
        M = sa.SA_F64[Mx, My, Mz]

        rb = mp.RigidBodyFixedCoM(mass, J, F, M)
        xₒ = [0, 0, 0.0]
        vₒ = [0, 0, 0.0]
        qₒ = [1.0, 0, 0, 0.0]
        ωₒ = [0.1, -0.2, 0.05]
        uₒ = [xₒ; vₒ; qₒ; ωₒ]

        # Also set up the rotation matrix case
        Rₒ = Quat(qₒ...)
        uo_rot = [xₒ; vₒ; ωₒ; Rₒ[:]]

        tf = 0.5
        tspan = (0.0, tf)
        prob_quat = ODEProblem(mp.simulate, uₒ, tspan, rb)
        sol_quat = solve(prob_quat, Tsit5(), saveat = 0.001)
        prob_rot = ODEProblem(mp.simulateRot, uo_rot, tspan, rb)
        sol_rot = solve(prob_rot, Tsit5(), saveat = 0.001)

        ϵ = 1e-7
        @test length(sol_quat.t) == length(sol_rot.t)
        for (t, u, urot) ∈ zip(sol_quat.t, sol_quat.u, sol_rot.u)
            sim_pos = u[mp.getPositionInd(rb)]
            sim_vel = u[mp.getVelocityInd(rb)]
            ωsim = u[mp.getAngularVelocityInd(rb)]

            qf_tmp = u[mp.getQuaternionInd(rb)]
            qf = Quat(qf_tmp...)
            r_xyz_quat = RotXYZ(qf)
            r_xyz_rot = RotXYZ(reshape(urot[10:18], (3, 3)))

            @test isapprox(norm(u[mp.getPositionInd(rb)] - urot[mp.getPositionInd(rb)]), 0.0, atol = ϵ)
            @test isapprox(norm(u[mp.getVelocityInd(rb)] - urot[mp.getVelocityInd(rb)]), 0.0, atol = ϵ)
            @test isapprox(norm(u[mp.getAngularVelocityInd(rb)] - urot[7:9]), 0.0, atol = 1e1 * ϵ)
            @test isapprox(norm(r_xyz_rot - r_xyz_quat), 0.0, atol = 1e1 * ϵ)
        end
    end
end

@testset "Math Helper Functions" begin
    @testset "Skew Matrix" begin
        a = [1, 2, 3]
        ahat = mp.vectorToSkew(a)
        @test ahat == -ahat'

        @test_throws AssertionError mp.vectorToSkew([1,2,3,4])
    end
end
