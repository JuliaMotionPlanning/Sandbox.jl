@testset "XMotorModel Tests" begin
    @testset "Dynamics" begin
        τ = 0.05
        k_f = 1
        k_m = 1
        quad_arm = 0.15
        params = mp.XMotorModelParams(τ, k_f, k_m, quad_arm)

        ωr = [5000, 6000, 7000, 8000.0]
        motor = mp.XMotorModel(params, ωr)

        u0 = zeros(4)
        tspan = (0.0, 1.0)

        prob = ODEProblem(mp.simulate, u0, tspan, motor)
        sol = solve(prob, Tsit5(), dtmax = 0.01)

        analytical_sol(t) = ωr .* (1.0 - exp(-t / τ))
        ua = analytical_sol.(sol.t)

        Δ = sum(sum(ua - sol.u))
        ϵ = 1e-3
        @test Δ < ϵ * length(sol.u) * length(sol.u[1])
    end
    @testset "Mixer" begin
        τ = 0.05
        k_f = 1
        k_m = 1
        quad_arm = 0.15
        params = mp.XMotorModelParams(τ, k_f, k_m, quad_arm)

        motor = mp.XMotorModel(params)

        motor.ω = [5000, 0, 0, 0.0]
        F1, _ = mp.compute_body_wrench(motor)

        motor.ω = [5000, 5000, 0, 0.0]
        F2, _ = mp.compute_body_wrench(motor)

        motor.ω = [5000, 5000, 5000, 0.0]
        F3, _ = mp.compute_body_wrench(motor)

        motor.ω = [5000, 5000, 5000, 5000.0]
        F4, M4 = mp.compute_body_wrench(motor)

        @test F2 == 2 * F1
        @test F3 == 3 * F1
        @test F4 == 4 * F1
        @test F4[1] ≈ 0
        @test F4[2] ≈ 0
        # Since RPM is 5k per motor, there should be no roll, pitch, or yaw moment.
        @test iszero(M4)

        # Motors 1 & 2 spin the same way. Located forward-right and back-left.
        motor.ω = [5000, 5000, 0, 0.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] ≈ 0
        @test M[2] ≈ 0
        @test M[3] > 0

        # Motors 3 & 4 spin the same way. Located forward-left and back-right.
        motor.ω = [0, 0, 5000, 5000.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] ≈ 0
        @test M[2] ≈ 0
        @test M[3] < 0

        # Motors 2 & 4 are in the back -- should pitch forwards, which is -Y.
        motor.ω = [0, 5000, 0, 5000.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] ≈ 0
        @test M[2] < 0
        @test M[3] ≈ 0

        # Motors 1 & 3 are in the front -- should pitch backwards, which is +Y.
        motor.ω = [5000, 0, 5000, 0.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] ≈ 0
        @test M[2] > 0
        @test M[3] ≈ 0

        # Motors 1 & 4 are on the right-side of the quad -- should roll to the left.
        motor.ω = [5000, 0, 0, 5000.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] < 0
        @test M[2] ≈ 0
        @test M[3] ≈ 0

        # Motors 2 & 3 are on the left-side of the quad -- should roll to the right.
        motor.ω = [0, 5000, 5000, 0.0]
        F, M = mp.compute_body_wrench(motor)
        @test M[1] > 0
        @test M[2] ≈ 0
        @test M[3] ≈ 0
    end

    @testset "Utilities" begin
        motor = mp.FakeMotorModel()
        @test mp.sizeStateVector(motor) == 1

        τ = 0.05
        k_f = 1
        k_m = 1
        quad_arm = 0.15
        params = mp.XMotorModelParams(τ, k_f, k_m, quad_arm)

        xmotor = mp.XMotorModel(params)
        @test mp.sizeStateVector(xmotor) == 4
    end
end
