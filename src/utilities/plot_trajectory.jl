#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    polyplot(polysol::PolySol; Δt = 0.1, kwargs...)
    polyplot(traj::PiecewiseTrajectory; Δt = 0.1, kwargs...)
    polyplot(traj::PolynomialTrajectory; Δt = 0.1, kwargs...)

Helper function to plot: [`PolyOpt`](@ref), [`PiecewiseTrajectory`](@ref), and [`PolynomialTrajectory`](@ref)

The trajectories are discretised at time steps of Δt. Plot arguments are forwarded to PlotlyJS for Layout's only.

!!! note
    Only valid for 2D or 3D trajectories.
"""
function polyplot(polysol::PolySol; Δt = 0.1, kwargs...)
    dimension = polysol.dimension
    if (dimension < 2) || (dimension > 3)
        error("polyplot only supports dimension 2 or 3 -- instead, found dimension of $dimension")
    end
    polyplot(polysol.trajectory; Δt = Δt, kwargs...)
end

function polyplot(polytraj::PiecewiseTrajectory{T}; Δt = 0.1, kwargs...) where {T <: AbstractFloat}
    dimension = num_dimensions(polytraj)
    if (dimension < 2) || (dimension > 3)
        error("polyplot only supports dimension 2 or 3 -- instead, found dimension of $dimension")
    end
    plotfunc = dimension == 3 ? PlotlyJS.scatter3d : PlotlyJS.scatter
    traces = Vector{GenericTrace}()
    for (index, traj) ∈ enumerate(polytraj.trajectories)
        plot_last_bookend = true
        plot_first_bookend = true
        if index == 1
            plot_last_bookend = false
        elseif index == length(polytraj.trajectories)
            plot_first_bookend = false
        end
        line, bookends = plottrace(traj; plot_first = plot_first_bookend, plot_last = plot_last_bookend, Δt = Δt, kwargs...)
        line[:showlegend] = false
        push!(traces, line)
        push!(traces, bookends)
    end
    traj_layout = Layout(;title="Trajectory with N = $(getN(polytraj)), segments = $(length(polytraj.trajectories))",
                        xaxis_title = "X",
                        yaxis_title = "Y",
                        zaxis_title = "Z",
                        showgrid = true,
                        showlegend = true,
                        kwargs...)
    PlotlyJS.plot(traces, traj_layout)
end

function polyplot(polytraj::PolynomialTrajectory{T}; Δt = 0.1, kwargs...) where {T <: AbstractFloat}
    dimension = num_dimensions(polytraj)
    if (dimension < 2) || (dimension > 3)
        error("polyplot only supports dimension 2 or 3 -- instead, found dimension of $dimension")
    end
    line, bookends = plottrace(polytraj; Δt = Δt, kwargs...)
    traj_layout = Layout(;title="Trajectory with N = $(getN(polytraj))",
                        xaxis_title = "X",
                        yaxis_title = "Y",
                        zaxis_title = "Z",
                        showgrid = true,
                        showlegend = false,
                        kwargs...)
    PlotlyJS.plot([line, bookends], traj_layout)
end

function plottrace(traj::AbstractTrajectory; plot_first = true, plot_last = true,
                   Δt = 0.1, kwargs...) where {T <: AbstractFloat}
    dimension = num_dimensions(traj)
    plotfunc = dimension == 3 ? PlotlyJS.scatter3d : PlotlyJS.scatter
    t, pos = discretise(traj, Δt)
    x = [position(x)[1] for x ∈ pos]
    y = [position(x)[2] for x ∈ pos]
    line = plotfunc(;x = x,
                    y = y,
                    mode = "lines",
                    line_color = Nord.blue)
    if plot_first && plot_last
        bookends = plotfunc(;x = [x[1], x[end]],
                            y = [y[1], y[end]],
                            mode = "markers",
                            showlegend = false,
                            marker_color = Nord.red)
    elseif plot_first
        bookends = plotfunc(;x = [x[1]],
                            y = [y[1]],
                            mode = "markers",
                            showlegend = true,
                            name = "Start",
                            marker_color = Nord.black)
    elseif plot_last
        bookends = plotfunc(;x = [x[end]],
                            y = [y[end]],
                            mode = "markers",
                            showlegend = true,
                            name = "Destination",
                            marker_color = Nord.green)
    end
    if dimension == 3
        z = [position(x)[3] for x ∈ pos]
        line[:z] = z
        if plot_first && plot_last
            bookends[:z] = [z[1], z[end]]
        elseif plot_first
            bookends[:z] = [z[1]]
        elseif plot_last
            bookends[:z] = [z[end]]
        end
    end
    return line, bookends
end

DerivativeUnits = Dict{Int, String}()
DerivativeUnits[1] = "[Distance]"
DerivativeUnits[2] = "[Distance / Time]"
DerivativeUnits[3] = "[Distance / Time^2]"
DerivativeUnits[4] = "[Distance / Time^3]"
DerivativeUnits[5] = "[Distance / Time^4]"
DerivativeUnits[6] = "[Distance / Time^5]"
DerivativeUnits[7] = "[Distance / Time^6]"
DerivativeUnits[8] = "[Distance / Time^7]"

function plot_dimensions(traj::AbstractTrajectory; Δt = 0.1, derivatives_to_plot = 3, kwargs...) where {T <: AbstractFloat}
    dimension = num_dimensions(traj)
    t, setpoints = discretise(traj, Δt)
    setpoints = to3DMatrix(setpoints)
    derivatives_to_plot = min(derivatives_to_plot, num_derivatives(traj))
    all_plots = []
    for dim ∈ 1:dimension
        plots = Vector{PlotlyBase.Plot{PlotlyBase.GenericTrace{Dict{Symbol,Any}}}}()
        for derivative ∈ 1:derivatives_to_plot
            val = setpoints[:, dim, derivative]
            line = PlotlyJS.scatter(;x = t,
                                     y = val,
                                     mode = "lines",
                                     line_color = Nord.blue,
                                     showlegend = false)
            traj_layout = Layout(;title="Trajectory, dimension = $dim, derivative = $(derivative-1)",
                                 xaxis_title = "Time [seconds]",
                                 yaxis_title = DerivativeUnits[derivative],
                                 showgrid = true,
                                 kwargs...)
            tmp_plot = PlotlyJS.Plot(line, traj_layout)
            push!(plots, tmp_plot)
        end
        vert_plots = vcat(plots...)
        display(plot(vert_plots))
        push!(all_plots, plots)
    end
    return all_plots
end
