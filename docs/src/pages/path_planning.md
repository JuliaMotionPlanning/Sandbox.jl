# Path Planning
```@index
Pages   = ["path_planning.md"]
```

## Benchmarks
### Free map, small
```@example benchmark_free_map_small
import MotionPlanning # hide
const mp = MotionPlanning # hide
using PlotlyJS # hide
using ORCA # hide
results = mp.gridmap_search_benchmark("free_map_small")
p_time = mp.plot_benchmark(results, test_name = "free_map_small",
                           benchmark_field = :time,
                           title = "Solve time vs. search strategy",
                           xaxis_title = "Search strategy",
                           yaxis_title = "Median solve time [μs]")
p_memory = mp.plot_benchmark(results, test_name = "free_map_small",
                             benchmark_field = :memory,
                             title = "Memory allocated vs. search strategy",
                             xaxis_title = "Search strategy",
                             yaxis_title = "Median memory allocated [MiB]")
savefig(p_time, "plot-time.svg"); nothing # hide
savefig(p_memory, "plot-memory.svg"); nothing # hide
```
![plot-time](plot-time.svg)
![plot-memory](plot-memory.svg)

## Gridmap Search
```@docs
MotionPlanning.bfs
MotionPlanning.dijkstra
MotionPlanning.astar
```

## Customisable Options
```@docs
MotionPlanning.get_neighbours
MotionPlanning.get_neighbours_4
MotionPlanning.get_neighbours_8
MotionPlanning.is_traversable
MotionPlanning.is_safe
MotionPlanning.is_safe_or_unknown
```
## Utilities
```@docs
MotionPlanning.get_cartesian_index
MotionPlanning.get_linear_index
MotionPlanning.get_position
MotionPlanning.on_map
MotionPlanning.indices2position
MotionPlanning.manhattan_distance
```
