#=
Copyright [2018] [Kyle Carbon]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
=#

"""
    Derivatives

Namespace for definitions related to derivatives:
* `const Derivative = Int64`
* `POSITION = ORIENTATION = 0`
* `VELOCITY = ANGULAR_VELOCITY = 1`
* `ACCELERATION = ANGULAR_ACCELERATION = 2`
* `JERK = 3`
* `SNAP = 4`
"""
module Derivatives
const Derivative = Int64

const POSITION = 0
const VELOCITY = 1
const ACCELERATION = 2
const JERK = 3
const SNAP = 4

const ORIENTATION = 0
const ANGULAR_VELOCITY = 1
const ANGULAR_ACCELERATION = 2

end # end Derivatives